from django.conf import settings
from django import template
register = template.Library()


@register.filter
def EDMS(value):
    """Check ob Buch vorhanden ist"""
    # value ist user für Rechte eines Users
    return settings.EDMS
