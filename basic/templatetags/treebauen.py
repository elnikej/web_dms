from webdms.models import Url
from django import template
register = template.Library()


@register.filter
def zweig(value):
    """JS Tree Zweig zusammen bauen"""
    if '/' in value.pfad:
        zw = '<ul><li id="' + value.slug + '">' + value.pfad + '</li><li>' + value.pfad + '</li></ul>'
    else:
        zw = value.pfad
    return zw


@register.filter
def teil(value):
    if '/' in value:
        zwvalue = str(value).split('/')
        zw = zwvalue[-1]
    else:
        zw = value
    return zw


@register.filter
def hassub1(value):
    """
    Prüfen ob Menü Sumbenüs hat, wenn ja dann True zurück geben (Für Ebene 0 mit Sub1)
    :param value:
    :return:
    """
    if Url.objects.filter(book=value.book).filter(parent=value.pfad).filter(ebene=1).exists():
        wert = True
    else:
        wert = False
    return wert


@register.filter
def hassub2(value):
    """
    Prüfen ob Menü Sumbenüs hat, wenn ja dann True zurück geben (Für Ebene 1 mit Sub2)
    :param value:
    :return:
    """
    if Url.objects.filter(book=value.book).filter(parent=value.pfad).filter(ebene=2).exists():
        wert = True
    else:
        wert = False
    return wert


@register.filter
def issub1(value):
    """
    SubMenü Level 1
    :param value:
    :return:
    """
    if Url.objects.filter(pk=value.pk).filter(ebene=1).exists():
        wert = True
    else:
        wert = False
    return wert


@register.filter
def issub2(value):
    """
    SubMenü Level 2
    :param value:
    :return:
    """
    if Url.objects.filter(pk=value.pk).filter(ebene=2).exists():
        wert = True
    else:
        wert = False
    return wert


@register.filter
def removeparent(value):
    """
    Parent entfernen; Rest zurück
    :param value:
    :return:
    """
    wert = value.pfad
    # par = value.parent
    return str(wert).replace(str(value.parent) + '/', ' ')
