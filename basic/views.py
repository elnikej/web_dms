from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse


def startpage(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('web_scribble_notes:index', args=[]))
    else:
        return HttpResponseRedirect(reverse('mysite_basic_login', args=[]))


def start(request):
    angemeldet = request.user.is_authenticated
    if angemeldet:
        return HttpResponseRedirect(reverse('web_scribble_notes:index', args=[]))
    dictionary = {
        'angemeldet': angemeldet,
        'error': False,
        'titel': 'Start',
    }
    return render(request, 'basic/start.html', dictionary)


def logoutpage(request):
    return render(request, 'basic/start.html', {'angemeldet': False, 'titel': 'abgemeldet', 'error': False})


def errorpage(request):
    if request.user.is_authenticated:
        return render(request, 'basic/keine-gruppe.html', {'angemeldet': False, 'error': False})
    else:
        return render(request, 'basic/start.html', {'angemeldet': False, 'titel': 'Fehler', 'error': True})
