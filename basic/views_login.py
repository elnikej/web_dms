from django.contrib import auth
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from .forms import UserForm


def basic_login(request):
    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            # Correct password, and the user is marked "active"
            auth.login(request, user)
            # Redirect to a success page.
            return HttpResponseRedirect(reverse('mysite_startpage', args=[]))
        else:
            # Show an error page
            return HttpResponseRedirect(reverse('mysite_errorpage', args=[]))
    elif request.user.is_authenticated:
        return HttpResponseRedirect(reverse('mysite_startpage', args=[]))
    else:
        form = UserForm()
        return render(request, 'basic/login.html', {'form': form,})


def basic_logout(request):
    auth.logout(request)
    # Redirect to a success page.
    # return HttpResponseRedirect("/account/loggedout/")
    return HttpResponseRedirect(reverse('mysite_logoutpage', args=[]))
