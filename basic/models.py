from django.db import models
from django.contrib.auth.models import User, Group


class Gruppe(models.Model):
    """
      zum festlegen, was die Hauptgruppe (zur Zeit ist)
      wenn Hauptuser, Admins mitglied mehrerer Gruppen sind und innerhalb einer Gruppe arbeiten sollen
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE, )
    group = models.ForeignKey(Group, on_delete=models.CASCADE, )

    def __str__(self):
        return str(self.user)

    class Meta:
        unique_together = ('user', 'group')
