from .models import User

# from django import forms
from django.forms import ModelForm


# nur für Lokale User in SSO auskommentieren
class UserForm(ModelForm):
    class Meta:
        model = User
        exclude = ('last_login', 'date_joined')
