from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import Gruppe


class GruppeResource(resources.ModelResource):
    class Meta:
        model = Gruppe


class GruppeAdmin(ImportExportModelAdmin):
    resource_class = GruppeResource
    list_display = ('user', 'group')
    list_filter = ('user', 'group')


admin.site.register(Gruppe, GruppeAdmin)
