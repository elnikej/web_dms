# from buchhaltung.jptools.string_helper import hauptgruppe, get_einstdbsjahr
# from buchhaltung.templatetags.rechte_helper import recht_system
from .forms import UserForm
from django.contrib.auth.models import User
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render


def user_anlegen(request):
    """
    :param request: Neuen Benutzer anlegen
    :return: FORM
    """
    # dbs = get_einstdbsjahr(request.user.id)
    # if not recht_system(request.user):
    #     return HttpResponseRedirect(reverse('mysite_fehler', args=["berechtigung", "3"]))
    # gruppe = hauptgruppe(request.user, dbs)
    pwerror = False
    userror = False
    fuser = ''
    if request.method == 'POST':
        datap = request.POST.copy()
        if datap['new_password1'] == datap['new_password2']:
            datap['password'] = datap['new_password1']
        else:
            pwerror = True
            # return HttpResponseRedirect(reverse('mysite_fehler', args=["password-missmatch", "21"]))
        if User.objects.filter(username=datap['username']).exists():
            userror = True
        # form = UserForm(user=request.user, data=datap)
        fuser = datap['username']
        form = UserForm(data=datap)
        if form.is_valid():
            # form.save()
            user = User.objects.create_user(username=form.cleaned_data['username'],
                                            # email='jlennon@beatles.com',
                                            password=form.cleaned_data['password'])
            return HttpResponseRedirect(reverse('mysite_verwaltung_user_activieren', args=[user.id]))
            # return HttpResponseRedirect(reverse('mysite_verwaltung_users', args=[]))
    else:
        form = UserForm()
    dictionary = {
        'pwerror': pwerror,
        'userror': userror,
        'fuser': fuser,
        'form': form,
        # 'primegroup': gruppe,
    }
    return render(request, 'basic/add_form.html', dictionary)


def user_password_setzen(request, userid):
    """
    :param request: Passwort setzen wenn es in beiden Form Feldern identisch ist
    :param userid: User (id)
    :return: FORM
    """
    # von https://docs.djangoproject.com/en/1.10/topics/auth/default/
    # u = User.objects.get(username='john')
    # u.set_password('new password')
    # u.save()
    # dbs = get_einstdbsjahr(request.user.id)
    # if not recht_system(request.user):
    #     return HttpResponseRedirect(reverse('mysite_fehler', args=["berechtigung", "3"]))
    # gruppe = hauptgruppe(request.user, dbs)
    pwerror = False
    fuser = User.objects.get(pk=userid)
    if request.method == 'POST':
        datap = request.POST.copy()
        if datap['new_password1'] == datap['new_password2']:
            datap['password'] = str(datap['new_password1']).strip()
            fuser.set_password(datap['password'])
            fuser.save()
            return HttpResponseRedirect(reverse('mysite_verwaltung_user_activieren', args=[fuser.id]))
        else:
            pwerror = True
            form = UserForm(data=datap)
    else:
        form = UserForm()
    dictionary = {
        'pwerror': pwerror,
        'form': form,
        # 'primegroup': gruppe,
        'formuser': fuser,
    }
    return render(request, 'basic/setpw_form.html', dictionary)


# Gruppen und Rechte
# https://www.geeksforgeeks.org/python-user-groups-custom-permissions-django/


# importing necessery django classes
# from django.contrib.auth.models import AbstractUser
# from django.utils import timezone
# from django.db import models
#
#
# # User class
# class User(AbstractUser):
#     # Define the extra fields
#     # related to User here
#     first_name = models.CharField(_('First Name of User'),
#                                   blank=True, max_length=20)
#
#     last_name = models.CharField(_('Last Name of User'),
#                                  blank=True, max_length=20)
#
#     # More User fields according to need
#
#     # define the custom permissions
#     # related to User.
#     class Meta:
#         permissions = (
#             ("can_go_in_non_ac_bus", "To provide non-AC Bus facility"),
#             ("can_go_in_ac_bus", "To provide AC-Bus facility"),
#             ("can_stay_ac-room", "To provide staying at AC room"),
#             ("can_stay_ac-room", "To provide staying at Non-AC room"),
#             ("can_go_dehradoon", "Trip to Dehradoon"),
#             ("can_go_mussoorie", "Trip to Mussoorie"),
#             ("can_go_haridwaar", "Trip to Haridwaar"),
#             ("can_go_rishikesh", "Trip to Rishikesh"),
#
#         # Add other custom permissions according to need.



        # importing group class from django
# from django.contrib.auth.models import Group, Permission
# from django.contrib.contenttypes.models import ContentType
#
# # import User model
# from users.models import User
#
# new_group, created = Group.objects.get_or_create(name='new_group')
#
# # Code to add permission to group
# ct = ContentType.objects.get_for_model(User)
#
# # If I want to add 'Can go Haridwar' permission to level0 ?
# permission = Permission.objects.create(codename='can_go_haridwar',
#                                        name='Can go to Haridwar',
#                                        content_type=ct)
# new_group.permissions.add(permission)
