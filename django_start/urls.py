"""django_start URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

from basic import views
from basic import views_login
from basic.views_fehler import error_500, error_404

from django.contrib.auth.views import PasswordChangeView, PasswordChangeDoneView


urlpatterns = [
    path('', views.start, name='start'),
    path('admin/', admin.site.urls),
    path('tinymce/', include('tinymce.urls')),
    path('login/', views_login.basic_login, name='mysite_basic_login'),
    path('logout/', views_login.basic_logout, name='mysite_basic_logout'),
    path('accounts/password_change/', PasswordChangeView.as_view(template_name='basic/password_change_form.html'),
         name='mysite_basic_change'),
    path('accounts/password_change_done/',
         PasswordChangeDoneView.as_view(template_name='basic/password_change_done.html'), name='password_change_done'),
    path('start/', views.startpage, name='mysite_startpage'),
    path('end/', views.logoutpage, name='mysite_logoutpage'),
    path('error/', views.errorpage, name='mysite_errorpage'),
    path('edms/', include('dmsapp.urls', namespace='dms')),
    path('webdms/', include('webdms.urls', namespace='web_scribble_notes')),
]

handler404 = error_404
handler500 = error_500
