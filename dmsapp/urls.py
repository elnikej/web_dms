from django.urls import path
from .views import file_import, datei_download

app_name = 'dms'

# dms
urlpatterns = [
    path('datei/<str:pathslug>/<str:dateislug>', datei_download, name='dateidownload'),
    path('fileimport/<slug:treeslug>', file_import, name='fileimport'),
]
