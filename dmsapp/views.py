from django.conf import settings
from django.contrib.auth.decorators import login_required
# from django.db import transaction, IntegrityError
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.text import slugify
from webdms.jptools.datei_helfer import handle_uploaded_media  # , try_utf8
from webdms.tools import booknavi, pfadnavi, geturlpath
from .forms import UploadFileFormFile  # , BelegForm
# from .models import Beleg
from webdms.models import Url, Dateien
from os import path, makedirs  # remove,
from sendfile import sendfile
from shutil import move

dmsroot = path.join(settings.SENDFILE_ROOT)


@login_required
def file_import(request, pathslug):
    """
    Dateien hinzufügen
    :param request:
    :param pathslug:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    if request.POST:
        datap = request.POST.copy()
        datap['gruppe'] = request.user.gruppe.group_id
        form = UploadFileFormFile(datap, request.FILES)
        if form.is_valid():
            full_path_dmsdatei = handle_uploaded_media(request.FILES['file'], request.user, '', 'upload')
            urlslug, urlpath = geturlpath(prime, pathslug)
            dmsrest = path.join(prime.slug, urlpath).lower()
            move_path_ziel = path.join(dmsroot, dmsrest)
            if not path.exists(move_path_ziel):
                makedirs(move_path_ziel)
            if path.isfile(full_path_dmsdatei):
                move(full_path_dmsdatei, move_path_ziel)
            # dateiname = 'xxx'
            # schluessel = None
            # try:
            #     urlid = Url.objects.filter(book=prime).get(slug=pathslug)
            # except Url.DoesNotExist:
            #     urlid = None
            Dateien.objects.create()
            if pathslug == 'None':
                return HttpResponseRedirect(reverse('web_scribble_notes:path:empty', args=[]))
            else:
                return HttpResponseRedirect(reverse('web_scribble_notes:path:view', args=[pathslug]))
    else:
        form = UploadFileFormFile()
    restorelist = booklist
    dictionary = {
        'form': form,
        'booklist': booklist,
        'book': prime,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'restorelist': restorelist,
        'pathlist': pathlist,
    }
    return render(request, 'file_import.html', dictionary)


@login_required
def datei_download(request, pathslug, dateislug):
    """
    :param request:
    :param pathslug:
    :param dateislug:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    # pathlist, emptylist = pfadnavi(prime)
    if not prime:
        return HttpResponseRedirect(reverse('mysite_oauth:logout', args=[]))
    try:
        urlpath = Url.objects.filter(book=prime).get(slug=pathslug)
    except Url.DoesNotExist:
        return HttpResponseRedirect(reverse('web_scribble_notes:index', args=[]))
    #
    try:
        datei = Dateien.objects.filter(pfad__slug=pathslug).get(slug=dateislug)
        datei.is_active = True
        datei.save(update_fields=['is_active'])
    except Dateien.DoesNotExist:
        if pathslug == 'None':
            try:
                datei = Dateien.objects.filter(pfad__isnull=True).get(slug=dateislug)
            except Dateien.DoesNotExist:
                return HttpResponseRedirect(reverse('web_scribble_notes:index', args=[]))
        else:
            return HttpResponseRedirect(reverse('web_scribble_notes:index', args=[]))
    # 
    grupslug = slugify(request.user.gruppe.group.name)
    file_full_path = path.join(dmsroot, grupslug, urlpath.filesubpath, datei.dmsname)
    # option for download with dmsname ?
    # downloadname = datei.dmsname
    return sendfile(request, file_full_path, True, datei.label)
