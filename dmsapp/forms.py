from django import forms
from django.utils.text import slugify


def validate_file_all(value):
    # if not value.name.lower().endswith('.xml'):
    #     raise forms.ValidationError("Es können nur XML-Dateien eingelesen werden.")
    pass


class UploadFileFormFile(forms.Form):
    # title = forms.CharField(max_length=250)
    file = forms.FileField(label='Bitte eine Datei auswählen', validators=[validate_file_all])
