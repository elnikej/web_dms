from django import forms
from .models import Books, Schluessel, Url
from django.utils.text import slugify
import itertools


class BooksForm(forms.ModelForm):
    class Meta:
        model = Books
        exclude = ('date_created', 'date_updated')

    def __init__(self, **kwargs):
        self.__user = kwargs.pop('user', None)
        super(BooksForm, self).__init__(**kwargs)

    def save(self, commit=True):
        if self.instance.pk is None:
            if self.__user is None:
                raise TypeError("You didn't give an user argument to the constructor.")
            self.instance.slug = slugify(self.instance.label)
            self.instance.author = self.__user
        return super(BooksForm, self).save(commit)


class SchluesselForm(forms.ModelForm):
    class Meta:
        model = Schluessel
        exclude = ('date_created', 'date_updated')

    def __init__(self, **kwargs):
        self.__user = kwargs.pop('user', None)
        super(SchluesselForm, self).__init__(**kwargs)

    def save(self, commit=True):
        if self.instance.pk is None:
            if self.__user is None:
                raise TypeError("You didn't give an user argument to the constructor.")
            self.instance.slug = slugify(self.instance.keyword)
            self.instance.author = self.__user
        return super(SchluesselForm, self).save(commit)


class UrlForm(forms.ModelForm):
    class Meta:
        model = Url
        exclude = ('date_created', 'date_updated')

    def __init__(self, **kwargs):
        # def __init__(self, *args, **kwargs):
        # if kwargs['instance'] is not None:
        try:
            if Url.objects.filter(book=kwargs['data']['book']).filter(slug=kwargs['data']['slug']).exists():
                orig = kwslug = kwargs['data']['slug']
                if '-1' in kwslug:
                    position = kwslug.index('-1')
                    orig = kwslug[:position]
                for x in itertools.count(1):
                    if not Url.objects.filter(slug=kwslug).exists():
                        break
                    kwslug = '%s-%d' % (orig, x)
                kwargs['data']['slug'] = kwslug
        except KeyError:
            pass
        self.__user = kwargs.pop('user', None)
        super(UrlForm, self).__init__(**kwargs)

    def save(self, commit=True):
        if self.instance.pk is None:
            if self.__user is None:
                raise TypeError("You didn't give an user argument to the constructor.")
            self.instance.slug = slugify(self.instance.child)
            self.instance.author = self.__user
        return super(UrlForm, self).save(commit)


def validate_file_7z(value):
    if not value.name.lower().endswith('.7z'):
        raise forms.ValidationError("Es können nur 7z-Dateien eingelesen werden.")


class UploadFileFormWeb7z(forms.Form):
    # title = forms.CharField(max_length=250)
    file = forms.FileField(label='Bitte eine 7z-Datei auswählen', validators=[validate_file_7z])
