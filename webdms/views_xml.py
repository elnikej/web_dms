from datetime import datetime
# import pytz
from xml.etree import cElementTree as ElementTree
from django.conf import settings
from django.db import IntegrityError
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import BooksTemp, Books, Url, Dateien, Schluessel, Schluessel2Datei
from os import path

dmsroot = path.join(settings.SENDFILE_ROOT)


def books_import_xml(datei, gruppeid, grupslug):
    """
    Verwaltung Artikel in Tabelle ArtikelTemp (Vorschau) einlesen
    :param datei: Datei zum einlesen
    :param gruppeid: Gruppe (id)
    :param grupslug: Gruppe (slug)
    :return: int 0 == OK
    """
    tree = ElementTree.parse(datei)
    version = tree.find('version')
    try:
        vervariante = version.find('variante')
    except IndexError:
        return HttpResponseRedirect(reverse('webdms:error',
                                            args=["artikel_einlesen",
                                                  "01",
                                                  'Unbekanntes_XML-Format']))
    verversion = version.find('version')
    # verdatumtz = datetime.strptime(version.find('datum').text, "%Y-%m-%d %H:%M:%S %Z%z")
    if verversion.text == '2019.12.16' and vervariante.text == 'Web-DMS':
        importok = True
    else:
        importok = False
    vervariante.clear()
    verversion.clear()
    buchdirectory = ''
    resultat = 22  # True
    if importok:
        xmltags = tree.find('booklist')
        buecher = xmltags.findall('books')
        for bu in buecher:
            buch = bu.find('book')
            # buchid = buch.find('id').text
            buchname = buch.find('book').text
            buchslug = buch.find('slug').text
            # buchgruppe = buch.find('gruppe').text
            # buchgruppeid = buch.find('gruppeid').text
            buchdirectory = buch.find('directory').text
            if path.exists(path.join(dmsroot, grupslug, buchdirectory)):
                resultat = 23 # True
                break
            buchroot = buch.find('bookroot').text
            # preview with BooksTemp ?
            # urlcount = len(bu.find('urlpaths').findall('urlpath'))
            # filecount = len(bu.find('dateien').findall('dateien'))
            # keywordcount = len(bu.find('schluessel').findall('schluessel'))
            # keyusedcount = len(bu.find('schluessel2datei').findall('schluessel2datei'))
            #
            # newbooktemp = BooksTemp.objects.create(book=buchname,
            #                                        bookimpid=buchid,
            #                                        slug=buchslug,
            #                                        directory=buchdirectory,
            #                                        bookroot=buchroot,
            #                                        gruppe_id=gruppeid,
            #                                        datum_xml=verdatumtz)
            newbook = Books.objects.create(book=buchname,
                                           slug=buchslug,
                                           gruppe_id=gruppeid,
                                           bookroot=buchroot,  # später durch reales ersetzen
                                           directory=buchdirectory)
            resultat = 0  # False
            if resultat == 0:
                # Books.objects.create(book=buchname,
                #                      slug=buchslug,
                #                      gruppe_id=gruppeid,
                #                      bookroot=buchroot,
                #                      directory=buchdirectory,
                #                      datum_xml=verdatumtz)
                urlpaths = bu.find('urlpaths').findall('urlpfad')
                for pfad in urlpaths:
                    # pfad = up.find('urlpfad')
                    pfadid = pfad.find('id').text
                    pfadslug = pfad.find('slug').text
                    pfadname = pfad.find('pfad').text
                    pfadebene = pfad.find('ebene').text
                    pfadauf = pfad.find('auf').text
                    pfadparent = pfad.find('parent').text
                    if pfadparent is None:
                        pfadparent = ''
                    pfadfilesubpath = pfad.find('filesubpath').text
                    Url.objects.create(book=newbook,
                                       slug=pfadslug,
                                       pfad=pfadname,  # IntegrityError - UNIQUE constraint failed: webdms_url.pfad
                                       ebene=pfadebene,
                                       auf=pfadauf,
                                       parent=pfadparent,
                                       filesubpath=pfadfilesubpath,
                                       urlimpid=pfadid)
                dateien = bu.find('dateien').findall('dateien')
                for fdt in dateien:
                    # fdt = dt.find('dateien')
                    dtid = fdt.find('id').text
                    # dtbookid = fdt.find('bookid').text
                    dtslug = fdt.find('slug').text
                    dtlabel = fdt.find('label').text
                    dtdmsname = fdt.find('dmsname').text
                    dthinweis = fdt.find('hinweis').text
                    if dthinweis is None:
                        dthinweis = ''
                    dtpfadid = fdt.find('pfadid').text  # Url.urlimpid
                    try:
                        urlpfad = Url.objects.get(urlimpid=dtpfadid)
                    except Url.DoesNotExist:
                        urlpfad = None
                    Dateien.objects.create(book=newbook,
                                           slug=dtslug,
                                           label=dtlabel,
                                           dmsname=dtdmsname,
                                           hinweis=dthinweis,
                                           # pfad_id=dtpfadid,
                                           pfad=urlpfad,
                                           dateiimpid=dtid)
                xmlschluessel = bu.find('schluessel').findall('schluessel')
                for xsch in xmlschluessel:
                    # xsch = schl.find('schluessel')
                    xschid = xsch.find('id').text
                    # xschbookid = xsch.find('bookid').text
                    xschslug = xsch.find('slug').text
                    xschluessel = xsch.find('schluessel').text
                    # xschgruppe = xsch.find('gruppe').text
                    Schluessel.objects.create(book=newbook,
                                              schluessel=xschluessel,
                                              slug=xschslug,
                                              gruppe_id=gruppeid,
                                              schluesselimpid=xschid)
                xmlschl2datei = bu.find('schluessel2datei').findall('schluessel2datei')
                for xsch2d in xmlschl2datei:
                    # xsch2d = schl2d.find('schluessel2datei')
                    # sch2did = xsch2d.find('id').text
                    sch2dschluesselid = xsch2d.find('schluesselid').text
                    sch2dateiid = xsch2d.find('dateiid').text
                    try:
                        dtneu = Dateien.objects.get(dateiimpid=sch2dateiid)
                    except Dateien.DoesNotExist:
                        continue
                    try:
                        schlneu = Schluessel.objects.get(schluesselimpid=sch2dschluesselid)
                    except Dateien.DoesNotExist:
                        continue
                    Schluessel2Datei.objects.create(datei_id=dtneu.pk,  # sch2dateiid,
                                                    schluessel_id=schlneu.pk)  # sch2dschluesselid)
                # cleaning importid's
                for urlcl in Url.objects.filter(urlimpid__isnull=False):
                    urlcl.urlimpid = None
                    urlcl.save(update_fields=['urlimpid'])
                for dtcl in Dateien.objects.filter(dateiimpid__isnull=False):
                    dtcl.dateiimpid = None
                    dtcl.save(update_fields=['dateiimpid'])
                for schlcl in Schluessel.objects.filter(schluesselimpid__isnull=False):
                    schlcl.schluesselimpid = None
                    schlcl.save(update_fields=['schluesselimpid'])
    return resultat, buchdirectory


def books_import_temp(gruppeid, bookstemplist, variante):
    """
    Books anlegen aus Bookstemp
    :param gruppeid:
    :param bookstemplist:
    :param variante: neu / delete / zusammen
    :return:
    """
    for imp in BooksTemp.objects.filter(gruppe_id=gruppeid).filter(pk__in=bookstemplist):
        # print(imp)
        # book_import(gruppeid, imp)
        if variante == 'delete':
            # Book sichern
            # imp.pk kommt von BooksTemp
            bk = Books.objects.filter(book=imp.book)  # imp.pk ändert sich nach einem import
            bk.delete()
            continue
        if variante == 'neu':
            buch = Books.objects.create(book=imp.book,
                                        slug=imp.slug,
                                        gruppe_id=gruppeid,
                                        is_active=False)
        else:
            buch = Books.objects.get(book=imp.book)
        # Trees zum Buch anlegen aus xml holen
        # print(imp.pfad)
        xml = ElementTree.parse(imp.pfad)
        booklisttag = xml.find('booklist')
        bookstags = booklisttag.findall('books')
        for bu in bookstags:
            booktag = bu.find('book')
            if str(imp.bookimpid) != booktag.find('id').text:
                continue
            # hier nur bei gleicher ID
            # Tags zuerst anlegen, damit die Referenz vorhanden ist
            keywordslisttags = bu.find('keywords').findall('keyword')
            for tg in keywordslisttags:
                try:
                    kw = Schluessel.objects.filter(book=buch).filter(gruppe_id=gruppeid).get(keyword=tg.find(
                        'keyword').text)
                    kw.keywordimpid = tg.find('id').text
                    kw.save()
                except Schluessel.DoesNotExist:
                    Schluessel.objects.create(book=buch,
                                              gruppe_id=gruppeid,
                                              keyword=tg.find('keyword').text,
                                              slug=tg.find('slug').text,
                                              keywordimpid=tg.find('id').text)
                # except IntegrityError
            # Urlpaths
            urllisttags = bu.find('urlpaths').findall('urlpath')
            for ptu in urllisttags:
                # if ptu.find('direct').text == 'False':
                #     trdir = False
                # else:
                #     trdir = True
                try:
                    try:
                        parxml = ptu.find('parent').text
                    except KeyError:
                        parxml = ''
                    except TypeError:
                        parxml = ''
                    if parxml is None:
                        parxml = ''
                    try:
                        aufxml = ptu.find('auf').text
                    except KeyError:
                        aufxml = 1
                    except TypeError:
                        aufxml = 1
                    if aufxml is None:
                        aufxml = 1
                    Url.objects.create(book_id=buch.pk,
                                       slug=ptu.attrib['slug'],
                                       path=ptu.attrib['path'],
                                       ebene=ptu.attrib['ebene'],
                                       parent=parxml,  # ptu.attrib['parent'],
                                       auf=aufxml,  # ptu.attrib['auf'],
                                       urlimpid=ptu.find('id').text)
                except IntegrityError:
                    # Trees existiert schon und soll bei merge nicht neu erzeugt werden
                    # für die Zuordnung von Notizen in einen vorhandenen Trees muss treeimpid gefüllt werden
                    try:
                        # updtree = Trees.objects.filter(book_id=buch.pk).filter(parent=tre.attrib['parent']).get(
                        #     child=tre.attrib['child'])
                        # updtree.treeimpid = tre.find('id').text
                        # updtree.save(update_fields=['treeimpid'])
                        updurl = Url.objects.filter(book_id=buch.pk).get(slug=ptu.attrib['slug'])
                        # updurl.path = ptu.attrib['path']
                        # updurl.ebene = ptu.attrib['ebene']
                        updurl.urlimpid = ptu.find('id').text
                        updurl.save(update_fields=['urlimpid'])
                    except Url.DoesNotExist:
                        pass
            # Notes
            dateienlisttags = bu.find('dateien').findall('datei')
            for datei in dateienlisttags:
                # xmlbaumid = note.find('treeid').text  # None  # später den richtigen Baum hier ermitteln
                # # baumid = getbaumzuid(treeslisttags, xmlbaumid)  # import id beim anlegen speichern
                # if xmlbaumid == 'None':
                #     baumid = None
                # else:
                #     try:
                #         baumid = Trees.objects.get(treeimpid=xmlbaumid).pk
                #     except Trees.DoesNotExist:
                #         baumid = None
                xmlpathid = datei.find('pathid').text
                if xmlpathid == 'None':
                    pathid = None
                else:
                    try:
                        pathid = Url.objects.get(urlimpid=xmlpathid).pk
                    except Url.DoesNotExist:
                        pathid = None
                xmlkeywordid = datei.find('keyword').text
                if xmlkeywordid == 'None':
                    keywordid = None
                else:
                    try:
                        keywordid = Schluessel.objects.get(keywordimpid=xmlkeywordid).pk
                    except Schluessel.DoesNotExist:
                        keywordid = None
                Dateien.objects.create(book=buch,
                                       # tree_id=baumid,
                                       path_id=pathid,
                                       slug=datei.find('slug').text,
                                       label=datei.find('label').text,
                                       hinweis=datei.find('hinweis').text,
                                       keyword_id=keywordid)
        # Trees.objects.filter(treeimpid__isnull=False).update(treeimpid=None)
        Url.objects.filter(urlimpid__isnull=False).update(urlimpid=None)
        Schluessel.objects.filter(keywordimpid__isnull=False).update(keywordimpid=None)
        # Der import wird mehrfach aufgerufen, deshalb darf hier nicht gelöscht werden
