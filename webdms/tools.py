from .models import Books, Dateien, Schluessel, Url
# from dmsapp.models import Beleg
from django.db import IntegrityError
from django.db.models.functions import Lower
from django.utils.text import slugify


def booknavi(gruppeid):
    try:
        booklist = Books.objects.filter(gruppe_id=gruppeid).order_by(Lower('book'))
    except AttributeError:
        booklist = Books.objects.none()
    # .annotate(bookis=Lower('book').order_by(Lower('book')).values_list('book', flat=True)
    try:
        prime = booklist.get(is_active=True)
    except Books.DoesNotExist:
        prime = Books.objects.none()

    if prime is None:
        schluessellist = Schluessel.objects.none()
    else:
        schluessellist = Schluessel.objects.filter(book__in=booklist)
    return schluessellist, booklist, prime


def changeurl_ebene0(prime, oldpath, urlpath, urlslug):
    """
    :param prime:
    :param oldpath:
    :param urlpath:
    :param urlslug:
    :return:
    """
    try:
        for urlp1 in Url.objects.filter(book=prime).filter(ebene=1).filter(parent=oldpath):
            secondoldparent = urlp1.path
            urlp1.parent = urlpath
            firstpathstr = str(urlp1.path).replace(oldpath, urlpath)
            urlp1.path = firstpathstr  # str(urlp.path).replace(oldpath, urlpath)
            # pathstr = DVDs (L 0)/Action
            urllist = str(firstpathstr).split('/')
            urlstr = ''
            for urlli in urllist:
                urlstr += slugify(urlli)
                if urlli != urllist[-1]:
                    urlstr += '.'
            urlp1.slug = urlstr
            urlp1.save()
            #
            for urlp2 in Url.objects.filter(book=prime).filter(ebene=2).filter(parent=secondoldparent):
                urlp2.parent = firstpathstr
                secondpathstr = str(urlp2.path).replace(oldpath, urlpath)
                urlp2.path = secondpathstr  # pathsstr2
                # slug zum neuen PATH erstellen
                secondpathlist = str(secondpathstr).split('/')
                urlstr2 = ''
                for urlli2 in secondpathlist:
                    urlstr2 += slugify(urlli2)
                    if urlli2 != secondpathlist[-1]:
                        urlstr2 += '.'
                urlp2.slug = urlstr2
                urlp2.save()
    except Url.DoesNotExist:
        path = urlpath
        pathsl = urlslug
        try:
            Url.objects.create(book=prime,
                               slug=pathsl,
                               path=path,
                               ebene=0,
                               auf=1,
                               parent='')
        except IntegrityError:
            pass


def changeurl_ebene1(urlid, oldpath):  # , oldparent):
    """
    :param urlid: URL Pfad der geändert wurde (Ebene 1)
    :param oldpath: path vor der Änderung
    param oldparent: parent vor der Änderung
    :return:
    """
    try:
        urlpath = Url.objects.get(pk=urlid)
        urlpathlist = str(urlpath.pfad).split('/')
        urlstr = ''
        for urlli in urlpathlist:
            urlstr += slugify(urlli)
            if urlli != urlpathlist[-1]:
                urlstr += '.'
        urlpath.slug = urlstr
        urlpath.save()
        # Ebene 2, falls forhanden
        changeurl_ebene2(urlpath, oldpath)  # , oldparent)
    except Url.DoesNotExist:
        pass


def changeurl_ebene2(urlpath, oldpath):  # , oldparent):
    """
    :param urlpath:
    :param oldpath:
    :return:
    """
    for urlp2 in Url.objects.filter(book=urlpath.book).filter(ebene=2).filter(parent=oldpath):
        urlp2.parent = urlpath.path  # firstpathstr
        secondpath = urlp2.path
        secondpathstr = str(secondpath).replace(oldpath, urlpath.path)
        urlp2.path = secondpathstr  # pathsstr2
        # slug zum neuen PATH erstellen
        secondpathlist = str(secondpathstr).split('/')
        urlstr2 = ''
        for urlli2 in secondpathlist:
            urlstr2 += slugify(urlli2)
            if urlli2 != secondpathlist[-1]:
                urlstr2 += '.'
        urlp2.slug = urlstr2
        urlp2.save()


def geturlpath(book, pathslug):
    """
    :param book:
    :param pathslug:
    :return:
    """
    try:
        urlpath = Url.objects.filter(book).get(slug=pathslug)
        slug = urlpath.slug
        path = urlpath.path
    except Url.DoesNotExist:
        slug = path = Url.objects.none()
    return slug, path


def pfadnavi(prime):
    """
    :param prime:
    :return:
    """
    if prime:
        pathlist = Url.objects.filter(book=prime)  # .order_by('slug')
        emptylist = Dateien.objects.filter(book=prime).exclude(pfad__isnull=False)
    else:
        pathlist = emptylist = Url.objects.none()
    return pathlist, emptylist
