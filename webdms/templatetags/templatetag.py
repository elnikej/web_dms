from webdms.models import Books, Dateien, Url, Schluessel2Datei, Schluessel
from django import template
register = template.Library()


@register.filter
def vorhanden(value):
    """Check ob Buch vorhanden ist"""
    return Books.objects.filter(book=value.book).exists()


@register.filter
def counturl(value):
    """Trees zu Book zählen"""
    return Url.objects.filter(book=value).count()


@register.filter
def countdateien(value):
    """Dateien zu Book zählen"""
    return Dateien.objects.filter(book=value).count()


@register.filter
def countschluessel(value):
    """count used Schluessel"""
    return Schluessel2Datei.objects.filter(schluessel=value).count()


@register.filter
def countbookschluessel(value):
    """Schluessel je Book"""
    dtbook = Dateien.objects.filter(book=value)
    return Schluessel2Datei.objects.filter(datei__in=dtbook).count()
