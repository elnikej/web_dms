from django.contrib.auth.decorators import login_required
# from django.core.exceptions import
from django.http import HttpResponseRedirect
from django.shortcuts import render  # , get_object_or_404
from django.urls import reverse
# from django.utils.text import slugify
from .models import Dateien  # , Url  # , Group  Books,
# from dmsapp.models import Beleg
from basic.models import Gruppe
# from .pathurl import noteurl, belegurl
from .tools import booknavi, pfadnavi


def index_view(request):
    """
    Stertseite mit Menü auf Linker Seote
    :param request:
    :return:
    """
    if request.user.is_authenticated:
        if Gruppe.objects.filter(user=request.user).count() == 0:
            # print('Fehler - keine gruppe in request.user')
            return HttpResponseRedirect(reverse('mysite_errorpage', args=[]))
        schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
        if not prime:
            return HttpResponseRedirect(reverse('webdms:bookview'))
        pathlist, emptylist = pfadnavi(prime)
        # emptylist = Notes.objects.filter(book=prime).exclude(tree__isnull=False)
        pathlist0 = pathlist  # .filter(ebene=0)
        # Prüfen ob es für das Book eine aktive Notiz gibt, wenn ja, dann laden
        try:
            datei = Dateien.objects.filter(book=prime).get(is_active=True)
            return HttpResponseRedirect(reverse('webdms:path:datei', args=[datei.pfad.slug, datei.slug]))
        except AttributeError:
            datei = Dateien.objects.filter(book=prime).get(is_active=True)
            datei.is_active = False
            datei.save(update_fields=['is_active'])
            return HttpResponseRedirect(reverse('webdms:index', args=[]))
        except Dateien.DoesNotExist:
            dictionary = {
                'booklist': booklist,
                'book': prime,
                'emptylist': emptylist,
                'pathlist': pathlist0,
                'schluessellist': schluessellist,
                'angemeldet': True,
                'error': False,
            }
            return render(request, 'webdms/start.html', dictionary)
    else:
        return HttpResponseRedirect(reverse('mysite_basic_login', args=[]))


@login_required
def menu_view(request):
    """
    Anzeige des Menü
    :param request:
    :return:
    """
    if Gruppe.objects.filter(user=request.user).count() == 0:
        # print('Fehler - keine gruppe in request.user')
        return HttpResponseRedirect(reverse('mysite_errorpage', args=[]))
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    if not prime:
        return HttpResponseRedirect(reverse('webdms:bookview'))
    pathlist, emptylist = pfadnavi(prime)
    # emptylist = Notes.objects.filter(book=prime).exclude(tree__isnull=False)
    pathlist0 = pathlist  # .filter(ebene=0)
    dictionary = {
        'booklist': booklist,
        'book': prime,
        'emptylist': emptylist,
        'pathlist': pathlist0,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
    }
    return render(request, 'webdms/menu.html', dictionary)


@login_required
def tree_empty(request):
    """
    Zur Anzeige von Notizen ohne Zuordnung
    :param request:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    notelist = emptylist
    dictionary = {
        'booklist': booklist,
        'book': prime,
        'notelist': notelist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'treeslug': None,
    }
    return render(request, 'tree_empty.html', dictionary)
