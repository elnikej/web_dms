from .models import Dateien, Schluessel, Schluessel2Datei, Url  # , Books
from xml.etree import cElementTree as ElementTree
# from datetime import datetime  # , timedelta, timezone
from os import path, makedirs  # , listdir
# from os.path import isfile, join
from django.conf import settings
from django.core.management.base import CommandError
# from django.http import HttpResponse  # HttpResponseRedirect,
from django.utils import timezone
import os
import shutil
import subprocess
# from py7zr import pack_7zarchvie, unpack_7zarchive

backupdir = path.join(settings.MEDIA_ROOT, 'backup')
dmsroot = path.join(settings.SENDFILE_ROOT)


def save_bookdb(bookbackuplist, grupslug, bckpord):
    """
        create db-backup of book in xml file
        :param bookbackuplist:
        :param grupslug: Gruppe (slug) für subdirectory
        :param bckpord: name of dir
        :return:
        """
    root = ElementTree.Element("xml")
    allg = ElementTree.SubElement(root, "version")
    xml_allg(allg, "Web-DMS", "2019.12.16", "Booklist")  # == KeyWords
    book_list = bookbackuplist
    xmlverw = ElementTree.SubElement(root, "booklist")

    for buch in book_list:
        xmlbooks = ElementTree.SubElement(xmlverw, "books", name=str(buch.book))
        xmlbk = ElementTree.SubElement(xmlbooks, 'book')
        ElementTree.SubElement(xmlbk, 'id').text = str(buch.pk)
        ElementTree.SubElement(xmlbk, 'book').text = str(buch.book)
        ElementTree.SubElement(xmlbk, 'slug').text = str(buch.slug)
        ElementTree.SubElement(xmlbk, 'gruppe').text = str(buch.gruppe)
        ElementTree.SubElement(xmlbk, 'gruppeid').text = str(buch.gruppe_id)
        ElementTree.SubElement(xmlbk, 'is_active').text = str(buch.is_active)
        ElementTree.SubElement(xmlbk, 'bookroot').text = str(buch.bookroot)
        ElementTree.SubElement(xmlbk, 'directory').text = str(buch.directory)
        ElementTree.SubElement(xmlbk, 'datum_updated').text = str(buch.datum_updated)
        ElementTree.SubElement(xmlbk, 'datum_created').text = str(buch.datum_created)
        xmltreeinh = ElementTree.SubElement(xmlbooks, "urlpaths")
        for ptu in Url.objects.filter(book=buch):
            xmltree = ElementTree.SubElement(xmltreeinh, "urlpfad", ebene=str(ptu.ebene), slug=str(ptu.slug),
                                             path=str(ptu.pfad))
            ElementTree.SubElement(xmltree, 'id').text = str(ptu.pk)
            # ElementTree.SubElement(xmltree, 'book').text = str(ptu.book)
            ElementTree.SubElement(xmltree, 'slug').text = str(ptu.slug)
            ElementTree.SubElement(xmltree, 'pfad').text = str(ptu.pfad)
            ElementTree.SubElement(xmltree, 'ebene').text = str(ptu.ebene)
            ElementTree.SubElement(xmltree, 'auf').text = str(ptu.auf)
            ElementTree.SubElement(xmltree, 'parent').text = str(ptu.parent)
            ElementTree.SubElement(xmltree, 'filesubpath').text = str(ptu.filesubpath)
            ElementTree.SubElement(xmltree, 'datum_updated').text = str(ptu.datum_updated)
            ElementTree.SubElement(xmltree, 'datum_created').text = str(ptu.datum_created)
        xmldateinh = ElementTree.SubElement(xmlbooks, 'dateien')
        for dtlb in Dateien.objects.filter(book=buch):
            xmldtlb = ElementTree.SubElement(xmldateinh, 'dateien', name=str(dtlb.label))
            ElementTree.SubElement(xmldtlb, 'id').text = str(dtlb.pk)
            ElementTree.SubElement(xmldtlb, 'bookid', name=str(dtlb.book.book)).text = str(dtlb.book_id)
            ElementTree.SubElement(xmldtlb, 'slug').text = str(dtlb.slug)
            ElementTree.SubElement(xmldtlb, 'label').text = str(dtlb.label)
            ElementTree.SubElement(xmldtlb, 'dmsname').text = str(dtlb.dmsname)
            ElementTree.SubElement(xmldtlb, 'hinweis').text = str(dtlb.hinweis)
            if dtlb.pfad is None:
                dateipath = ''
            else:
                dateipath = str(dtlb.pfad.pfad)
            ElementTree.SubElement(xmldtlb, 'pfadid', name=dateipath).text = str(dtlb.pfad_id)
            ElementTree.SubElement(xmldtlb, 'is_active').text = str(dtlb.is_active)
            ElementTree.SubElement(xmldtlb, 'datum_updated').text = str(dtlb.datum_updated)
            ElementTree.SubElement(xmldtlb, 'datum_created').text = str(dtlb.datum_created)
        xmltaginh = ElementTree.SubElement(xmlbooks, "schluessel")
        for key in Schluessel.objects.all():
            xmltag = ElementTree.SubElement(xmltaginh, 'schluessel', name=str(key.schluessel))
            ElementTree.SubElement(xmltag, 'id').text = str(key.pk)
            ElementTree.SubElement(xmltag, 'bookid').text = str(key.book_id)
            ElementTree.SubElement(xmltag, 'schluessel').text = str(key.schluessel)
            ElementTree.SubElement(xmltag, 'slug').text = str(key.slug)
            ElementTree.SubElement(xmltag, 'gruppe').text = str(key.gruppe)
            ElementTree.SubElement(xmltag, 'datum_updated').text = str(key.datum_updated)
            ElementTree.SubElement(xmltag, 'datum_created').text = str(key.datum_created)
        xmls2dtinh = ElementTree.SubElement(xmlbooks, "schluessel2datei")
        # only used from current book
        dtbook = Dateien.objects.filter(book=buch)
        for key in Schluessel2Datei.objects.filter(datei__in=dtbook):
            xmlkey = ElementTree.SubElement(xmls2dtinh, 'schluessel2datei')
            ElementTree.SubElement(xmlkey, 'id').text = str(key.pk)
            ElementTree.SubElement(xmlkey, 'dateiid').text = str(key.datei.pk)
            ElementTree.SubElement(xmlkey, 'schluesselid').text = str(key.schluessel.pk)
    return xmlfile_end(root, grupslug, bckpord)


def save_bookdir(src, bckpdir):
    """
    copy files an create 7z
    :param src:
    :param bckpdir:
    :return:
    """
    # 1. dms Verzeichnis nach bckpdir kopieren
    copytree(src, bckpdir)
    # 2. 7z subprocess -- working
    siebenzipname = path.join(bckpdir + '.7z')
    try:
        cmd = ['/usr/bin/7z', 'a', siebenzipname, bckpdir, '-mx7', '-sdel', '-bso0']  # 3 für schneller
        proz = subprocess.Popen(cmd, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
        if proz.stdout:
            proz.stdout.close()
        if proz.stderr:
            proz.stderr.close()
        ergebnis = 'fertig'
    except FileNotFoundError:
        ergebnis = 'programm-7z-ist-nicht-vorhanden'
    except CommandError:
        ergebnis = 'komprimieren-sicherung'
    return ergebnis
    # 2. b 7z -- py7zr


def indent(elem, level=0):
    """
    Für Zeilenumbrüchen von
    http://stackoverflow.com/questions/3095434/
    inserting-newlines-in-xml-file-generated-via-xml-etree-elementtree-in-python
    :param elem:
    :param level:
    :return:
    """
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


def xml_allg(allg, variante, version, name):
    """
    :param allg: XMl Zweig allgeimn
    :param variante: Export Variante (Wert in Tag)
    :param version: Version, die in die XML geschrieben wird
    :param name:
    :return: xml mit Allgemein
    """
    ElementTree.SubElement(allg, "version", name="version").text = version
    ElementTree.SubElement(allg, "datum", name="datum").text = timezone.localtime(timezone.now()).strftime(
        "%Y-%m-%d %H:%M:%S %Z%z")
    ElementTree.SubElement(allg, "name", name="name").text = name
    ElementTree.SubElement(allg, "format", name="format").text = "XML"
    ElementTree.SubElement(allg, "variante", name="variante").text = variante


# def xml_end(root, name):
#     """
#     :param root: XMl root
#     :param name: name der XML Datei
#     :return: xml
#     """
#     tree = ElementTree.ElementTree(root)
#     indent(root)
#     response = HttpResponse(content_type='application/xml')
#     response['Content-Disposition'] = 'attachment; filename="' + name + '"'
#     tree.write(response, encoding="utf-8", xml_declaration=True)
#     return response


def xmlfile_end(root, grupslug, dateiname):
    """
    :param root: XMl root
    :param grupslug: Gruppe (slug) für subdirectory
    :param dateiname: name der XML Datei
    :return: xml
    """
    tree = ElementTree.ElementTree(root)
    indent(root)
    filedir = path.join(backupdir, grupslug, dateiname)
    if not path.exists(filedir):
        makedirs(filedir)
    datei = path.join(filedir, dateiname + '.xml')
    tree.write(datei, encoding="utf-8", xml_declaration=True)
    return True


def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)
