from .models import Dateien, Books
from webdms.wspath.form import DateiForm
from .tools import booknavi, pfadnavi
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.text import slugify


@login_required
def datei_add(request, treeslug):
    """
    Neue Notiz anlegen
    :param request:
    :param treeslug:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    if request.method == 'POST':
        datap = request.POST.copy()
        datap['book'] = prime.pk
        if datap['tree'] == 'None':
            datap['tree'] = None
        if datap['keyword'] == 'None':
            datap['keyword'] = None
        # datap['body'] = 'body'
        datap['slug'] = slugify(datap['label'])  # Prüfen ob das im model gemacht wird
        if datap['content'] == '':
            datap['content'] = '<p>&nbsp;</p>'
        form = DateiForm(data=datap)  # (request.POST, instance=note)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.owner = request.user
            obj.save()
    else:
        form = DateiForm(instance=None)
    dictionary = {
        'form': form,
        'booklist': booklist,
        'book': prime,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'treeslug': treeslug,
        'addnotiz': True,
    }
    return render(request, 'wspath/datei_add_form.html', dictionary)


@login_required
def datei_change(request, dateislug):
    """
    Vorhandene Notiz ändern
    :param request:
    :param dateislug:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    dateilistprime = Dateien.objects.filter(book=prime)
    try:
        datei = Dateien.objects.filter(book=prime).get(slug=dateislug)
    except Dateien.DoesNotExist:
        # gibt es nicht
        return HttpResponseRedirect(reverse('webdms:index', args=[]))
    if datei.pfad is None:
        pfadslug = None  # pfadid =
        dateilist = emptylist  # Notes.objects.filter(book=prime).filter(tree__isnull=False)
    else:
        pfadslug = datei.pfad.slug
        dateilist = dateilistprime.filter(tree_id=datei.pfad_id)
    if request.method == 'POST':
        datap = request.POST.copy()
        if datap['tree'] == 'None':
            datap['tree'] = None
            datap['book'] = datei.book_id
        datap['slug'] = slugify(datap['label'])  # Prüfen ob das im model gemacht wird
        if datap['content'] == '':
            datap['content'] = '<p>&nbsp;</p>'
        form = DateiForm(instance=datei, data=datap)  # (request.POST, instance=note)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.owner = request.user
            obj.save()
        if datei.pfad is None:
            pfadslug = None
        else:
            pfadslug = datei.pfad.slug
        return HttpResponseRedirect(reverse('webdms:noteview', args=[pfadslug, datei.slug]))
    else:
        form = DateiForm(instance=datei)
    dictionary = {
        'form': form,
        'datei': datei,
        'booklist': booklist,
        'book': prime,
        'dateilist': dateilist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'addnotiz': True,
        'pfadslug': pfadslug,
        'keyid': datei.keyword_id,
    }
    return render(request, 'wspath/datei_add_form.html', dictionary)


@login_required
def note_delete(request, noteslug):
    """
    Notiz zum Löschen anzeigen
    :param request:
    :param noteslug:
    :return:
    """
    try:
        datei = Dateien.objects.get(slug=noteslug)
    except Dateien.DoesNotExist:
        # gibt es nicht
        return HttpResponseRedirect(reverse('webdms:index', args=[]))
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    dateilistprime = Dateien.objects.filter(book=prime)
    if datei.pfad is None:
        pfadslug = None
        dateilist = emptylist  # Notes.objects.filter(book=prime).filter(tree__isnull=False)
    else:
        pfadslug = datei.pfad.slug
        dateilist = dateilistprime.filter(pfad_id=datei.pfad_id)
    if request.method == 'POST':
        datap = request.POST.copy()
        datap['book'] = datei.book_id  # prime.pk
        datap['slug'] = datei.slug  # Prüfen ob das im model gemacht wird
        form = DateiForm(instance=datei, data=datap)  # (request.POST, instance=note)
        if form.is_valid():
            datei.delete()
        if pfadslug is None:
            return HttpResponseRedirect(reverse('webdms:treeempty', args=[]))
        return HttpResponseRedirect(reverse('webdms:treeview', args=[pfadslug]))
    else:
        form = DateiForm(instance=datei)
    dictionary = {
        'form': form,
        'booklist': booklist,
        'book': prime,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'dateilist': dateilist,
        'angemeldet': True,
        'error': False,
        'pfadslug': pfadslug,  # note.tree.slug,
        'datei': datei,
        'loeschen': datei,
    }
    return render(request, 'wspath/datei_add_form.html', dictionary)


@login_required
def datei_fehler(request, pfadslug, fehlerid):
    """

    :param request:
    :param pfadslug:
    :param fehlerid:
    :return:
    """
    if str(fehlerid) == '1':
        labelerror = 'Fehler beim Anlegen einer Notiz'
        bodyerror = 'Es gibt schone eine Notiz mit dem gleichen Label an dieser Stelle.'
    elif str(fehlerid) == '2':
        labelerror = 'Fehler 2'
        bodyerror =  '?'  #'Es gibt bereits eine Datei mit diesem Namen an der Stelle.'
    elif str(fehlerid) == '3':
        labelerror = 'Fehler beim Speichern'
        bodyerror = 'Es wurde keine Datei zum speichern ausgewählt.'
    elif str(fehlerid) == '4':
        labelerror = 'Fehler beim Speichern'
        bodyerror = 'Die Datei wurde an dieser Stelle nicht gefunden.'
    else:
        labelerror = 'Unbekannte Fehlernummer'
        bodyerror = 'Keine Informationen zu dem Fehler vorhanden.'
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    dictionary = {
        'booklist': booklist,
        'book': prime,
        # 'dateilist': dateilist,
        'emptylist': emptylist,
        'pathlist': pathlist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'labelerror': labelerror,
        'bodyerror': bodyerror,
        'pfadslug': pfadslug,
    }
    return render(request, 'dateien-error.html', dictionary)


@login_required
def datei_view(request, pfadslug, dateislug):
    """
    Zeigt den Inhalt einer Notiz an (HTML - Lesen)
    :param request:
    :param pfadslug:
    :param dateislug:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    dateilistprime = Dateien.objects.filter(book=prime)
    # emptylist = notelistprime.exclude(tree__isnull=False)
    if pfadslug == 'None':
        dateilist = emptylist  # Notes.objects.filter(book=prime).exclude(tree__isnull=False)
        try:
            datei = dateilistprime.get(slug=dateislug)
        except Dateien.DoesNotExist:
            datei = Dateien.objects.none()
    else:
        dateilist = dateilistprime.filter(pfad__slug=pfadslug)
        try:
            datei = dateilistprime.filter(tree__slug=pfadslug).get(slug=dateislug)
        except Dateien.DoesNotExist:
            # Notiz gelöscht
            datei = Dateien.objects.none()
    dictionary = {
        'booklist': booklist,
        'book': prime,  # 'primebook': prime.book,
        'dateilist': dateilist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'dateislug': dateislug,
        'datei': datei,
    }
    return render(request, 'dateien.html', dictionary)


@login_required
def datei_view_book(request, bookslug, pfadslug, dateislug):
    """
    Bookewechseln und dann Datei anzeigen (für Aufruf von KeyWords)
    :param request:
    :param bookslug:
    :param pfadslug:
    :param dateislug:
    :return:
    """
    try:
        book = Books.objects.filter(gruppe_id=request.user.gruppe.group_id).get(slug=bookslug)
        book.is_active = True
        book.save()
    except Books.DoesNotExist:
        return HttpResponseRedirect(reverse('webdms:index', args=[]))
    return HttpResponseRedirect(reverse('webdms:noteview', args=[pfadslug, dateislug]))
