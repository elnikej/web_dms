from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import Books, BooksTemp, Dateien, Schluessel, Url


class BooksResources(resources.ModelResource):
    class Meta:
        model = Books


class BooksAdmin(ImportExportModelAdmin):
    resource_class = BooksResources
    prepopulated_fields = {"slug": ("book",)}
    list_display = ('id', 'gruppe', 'slug', 'book', 'is_active')
    list_filter = ('gruppe', 'book')


class BooksTempResources(resources.ModelResource):
    class Meta:
        model = BooksTemp


class BooksTempAdmin(ImportExportModelAdmin):
    resource_class = BooksTempResources
    # prepopulated_fields = {"slug": ("book",)}
    list_display = ('id', 'bookimpid', 'gruppe', 'slug', 'book', 'directory')
    list_filter = ('gruppe', 'book')


class DateienResources(resources.ModelResource):
    class Meta:
        model = Dateien


class DateienAdmin(ImportExportModelAdmin):
    resource_class = DateienResources
    prepopulated_fields = {"slug": ("label",)}
    list_display = ('book', 'label', 'slug')
    list_filter = ('book', 'is_active')


class SchluesselResources(resources.ModelResource):
    class Meta:
        model = Schluessel


class SchluesselAdmin(ImportExportModelAdmin):
    resource_class = SchluesselResources
    prepopulated_fields = {"slug": ("schluessel",)}
    list_display = ('book', 'schluessel', 'slug')
    list_filter = ('book', 'schluessel')


class UrlResources(resources.ModelResource):
    class Meta:
        model = Url


class UrlAdmin(ImportExportModelAdmin):
    resource_class = UrlResources
    # prepopulated_fields = {"slug": ("path",)}
    list_display = ('book', 'parent', 'slug', 'pfad', 'filesubpath', 'ebene', 'auf')
    list_filter = ('book', 'parent', 'pfad', 'ebene', 'auf')


admin.site.register(Books, BooksAdmin)
admin.site.register(BooksTemp, BooksTempAdmin)
admin.site.register(Dateien, DateienAdmin)
admin.site.register(Schluessel, SchluesselAdmin)
admin.site.register(Url, UrlAdmin)
