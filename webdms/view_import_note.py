from .models import Dateien, Url  # , Trees
from os.path import split, splitext
from django.utils.text import slugify


def notes_import(txtdatei, pathslug, bookid):
    """
    Funktion importiert die Datei und legt eine Notiz an
    :param txtdatei: Datei incl. Pfad
    :param pathslug:
    :param bookid:
    :return:
    """
    try:
        # treeid = Trees.objects.get(slug=treeslug).pk
        urlid = Url.objects.filter(book__id=bookid).get(slug=pathslug).pk
    except Url.DoesNotExist:
        urlid = None
    rtfpfad, rtffile = split(txtdatei)
    name, ext = splitext(rtffile)
    rtflabel = name[30:]  # -4]
    rtfslug = slugify(rtflabel)
    if str(ext).lower() == '.rtf':
        # inhalt = 'rtf'
        with open(txtdatei, 'r') as file:
            inhalt = file.read()
        # doc = Rtf15Reader.read(open(txtdatei, "rb"))
        # inhalt = XHTMLWriter.write(doc, pretty=True).read()
        # with open(txtdatei, 'rb') as file:
        # inhaltpd = pandoc.Document()
        # inhaltpd.markdown = ''' '''
        # inhaltpd.rtf = inhaltrtf
        # inhalt = inhaltpd.html
        # print(inhalt)
        # inhalt = rtf2text(inhaltrtf)
        # inhalt = striprtf(inhaltrtf) -- funktioniert auch nicht
        # inhalt = strip_rtf(txtdatei, verbose = False)  ??
    elif str(ext).lower() == '.html' or str(ext).lower() == '.htm':
        with open(txtdatei, 'r') as file:
            inhalt = file.read()
    elif str(ext).lower() == '.txt':
        with open(txtdatei, 'r') as file:
            inhalt = file.read()  # .replace('\n', '')
    else:
        inhalt = 'Unbekanntest Format. Inhalt konnte nicht importiert werden.'
    Dateien.objects.create(book_id=bookid, path_id=urlid, slug=rtfslug, label=rtflabel, content=inhalt)
