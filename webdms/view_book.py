from .models import Books, Dateien, BooksTemp, Group, Url
from django.conf import settings
from django.contrib.auth.decorators import login_required
# from django.core.management.base import CommandError
# from django.db import transaction, IntegrityError
from django.db.models.functions import Lower
from django.http import HttpResponseRedirect
from django.shortcuts import render  # , get_object_or_404
from django.urls import reverse
from django.utils.text import slugify
from .tools import booknavi, pfadnavi
from .forms import BooksForm, UploadFileFormWeb7z
from .backup import save_bookdb, save_bookdir  # , sicherung_book
from .views_xml import books_import_xml, books_import_temp
from webdms.jptools.datei_helfer import handle_uploaded_media  # , try_utf8
from datetime import datetime
from os import path, makedirs, listdir, rename, rmdir, remove
from os.path import isfile, join
from sendfile import sendfile
from shutil import move, rmtree
# import subprocess
from py7zr import unpack_7zarchive

backupdir = path.join(settings.MEDIA_ROOT, 'backup')
dmsroot = path.join(settings.SENDFILE_ROOT)


@login_required
def book_view(request):
    """
    Alle Bücher anzeigen
    :param request:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    if prime:
        pass  # primebook = prime  # .book
    else:
        if booklist.count() > 0:
            # correction
            first = Books.objects.filter(gruppe_id=request.user.gruppe.group_id).first()
            first.is_active = True
            first.save()
            schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
            pathlist, emptylist = pfadnavi(prime)
    dictionary = {
        'book': prime,
        'booklist': booklist,
        'pathlist': pathlist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'titel': 'Books',
    }
    return render(request, 'books.html', dictionary)


@login_required
def book_viewtemp(request):
    """
    Alle Bücher anzeigen
    :param request:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    restorelist = BooksTemp.objects.filter(gruppe_id=request.user.gruppe.group_id).order_by(Lower('book'))
    dictionary = {
        'booklist': booklist,
        'book': prime,  # 'primebook': primebook,
        # 'treelist': treelist,
        'pathlist': pathlist,
        # 'directlist': directlist,
        # 'parentlist': parentlist,
        # 'childlist': childlist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'titel': 'Books Restore (Temp)',
        'restorelist': restorelist,
    }
    return render(request, 'bookstemp.html', dictionary)


@login_required
def book_add(request):
    """
    Books anlegen
    :param request:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    if request.POST:
        datap = request.POST.copy()
        datap['gruppe'] = request.user.gruppe.group_id
        datap['slug'] = slugify(datap['book'])
        # Wenn es noch kein Buch gibt, muss
        if Books.objects.filter(gruppe_id=request.user.gruppe.group_id).count() == 0:
            datap['is_active'] = True
        if not path.exists(dmsroot):
            makedirs(dmsroot)
        # if path.exists(mediadir):
        bookslug = slugify(datap['book'])
        grupslug = slugify(Group.objects.get(pk=datap['gruppe']).name)
        bookdir = path.join(dmsroot, grupslug, bookslug)
        if not path.exists(bookdir):
            makedirs(bookdir)
        datap['bookroot'] = dmsroot
        datap['directory'] = bookslug
        form = BooksForm(data=datap)  # (request.POST, instance=note)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.owner = request.user
            # hier Verzeichnis anlegen unterhalb von media
            # MEDIA_ROOT  // Name von Verzeichnis trennen
            # if not path.exists(mediadir):
            #     makedirs(mediadir)
            #     # if path.exists(mediadir):
            #     bookslug = slugify()
            #     bookdir = path.join(mediadir, bookslug)
            #     if not path.exists(bookdir):
            #         makedirs(bookdir)
            obj.save()
            return HttpResponseRedirect(reverse('webdms:bookview', args=[]))
    else:
        form = BooksForm(instance=None)
    dictionary = {
        'form': form,
        'booklist': booklist,
        'book': prime,  # 'primebook': primebook,
        # 'treelist': treelist,
        # 'directlist': directlist,
        # 'parentlist': parentlist,
        # 'childlist': childlist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'titel': 'Books',
        'addbook': True,
        'mygruppe': str(request.user.gruppe.group),
    }
    return render(request, 'books.html', dictionary)


@login_required
def book_edit(request, bookslug):
    """
    Buch ändern
    :param request:
    :param bookslug:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    try:
        book = Books.objects.get(slug=bookslug)
        oldbook = book.directory
    except Books.DoesNotExist:
        return HttpResponseRedirect(reverse('webdms:bookview', args=[]))
    if request.POST:
        datap = request.POST.copy()
        datap['gruppe'] = request.user.gruppe.group_id
        # datap['slug'] = slugify(datap['book'])
        newdir = slugify(datap['book'])
        datap['slug'] = newdir
        form = BooksForm(instance=book, data=datap)  # (request.POST, instance=note)
        if form.is_valid():
            # Welche Prüfungen vor dem Löschen ?
            grupslug = slugify(request.user.gruppe.group.name)
            if datap['subbook'] == 'Remove Book':
                rmpath = path.join(dmsroot, grupslug, str(oldbook).lower())
                try:
                    rmdir(rmpath)
                    book.delete()
                except OSError:
                    # Verzeichnis ist nicht leer
                    return HttpResponseRedirect(reverse('webdms:treeerror', args=[12]))
            else:
                obj = form.save(commit=False)
                obj.owner = request.user
                urllist = Url.objects.filter(book=obj)
                for ur in urllist:
                    fspath = str(ur.filesubpath).replace(obj.directory, newdir, 1)
                    ur.filesubpath = fspath  # str(ur.filesubpath).replace(obj.directory, newdir, 1)
                    ur.save(update_fields=['filesubpath'])
                # ~/git/dmsdir0/DEVELOP/dms/demo4/
                oldpath = path.join(dmsroot, grupslug, str(oldbook).lower())
                newbook = path.join(dmsroot, grupslug, str(newdir).lower())
                rename(oldpath, newbook)
                obj.directory = newdir  # datap['slug']
                obj.save()

            return HttpResponseRedirect(reverse('webdms:bookview', args=[]))
    else:
        form = BooksForm(instance=book)
    dictionary = {
        'form': form,
        'booklist': booklist,
        'book': prime,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'titel': 'Books',
        'addbook': True,
        'object': book,
        'mygruppe': str(request.user.gruppe.group),
    }
    return render(request, 'books.html', dictionary)


# @login_required
# def book_delete(request, bookslug):
#     """
#     Buch löschen mit Warnung dass noch Daten vorhanden sind
#     :param request:
#     :param bookslug:
#     :return:
#     """
#     schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
#     pathlist, emptylist = pfadnavi(prime)
#     try:
#         book = Books.objects.get(slug=bookslug)
#     except Books.DoesNotExist:
#         return HttpResponseRedirect(reverse('webdms:bookview', args=[]))
#     if request.POST:
#         datap = request.POST.copy()
#         datap['gruppe'] = request.user.gruppe.group_id
#         # bklist = Books.objects.filter(pk=datap['book'])
#         datap['book'] = book.book
#         form = BooksForm(instance=book, data=datap)  # (request.POST, instance=note)
#         if form.is_valid():
#             # Welche Prüfungen vor dem Löschen ?
#             if datap['subbook'] == 'Remove Book':
#                 xmlname = slugify(book.book) + datetime.now().strftime("__%Y-%m-%d_%H-%M-%S") + '.xml'
#                 # if sicherung_book(xmlname, bklist, 'file'):
#                 #     book.delete()
#             return HttpResponseRedirect(reverse('webdms:bookview', args=[]))
#     else:
#         form = BooksForm(instance=book)
#     bookdeleteslist = booklist.filter(slug=bookslug)
#     dictionary = {
#         'form': form,
#         'booklist': booklist,
#         'book': prime,
#         'emptylist': emptylist,
#         'schluessellist': schluessellist,
#         'angemeldet': True,
#         'error': False,
#         'titel': 'Books',
#         'addbook': True,
#         'bookdeleteslist': bookdeleteslist,
#         'object': book,
#         'mygruppe': str(request.user.gruppe.group),
#     }
#     return render(request, 'books_mit-inhalt.html', dictionary)


@login_required
def change_book(request, bookslug):
    """
    Book change / Haupt + Themenwechsel
    :param request:
    :param bookslug:
    :return:
    """
    try:
        # book = Books.objects.get(pk=bookid)
        book = Books.objects.filter(gruppe_id=request.user.gruppe.group_id).get(slug=bookslug)
        book.is_active = True
        book.save()
    except Books.DoesNotExist:
        return HttpResponseRedirect(reverse('webdms:index', args=[]))
    try:
        notiz = Dateien.objects.filter(book=book).get(is_active=True)
        return HttpResponseRedirect(reverse('webdms:path:datei', args=[notiz.pfad.slug, notiz.slug]))
    except Dateien.DoesNotExist:
        return HttpResponseRedirect(reverse('webdms:index', args=[]))


@login_required
def book_backup(request):  # , bookslug):
    """
    Aktives Book sichern
    :param request:
    param bookslug:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    try:
        book = Books.objects.filter(gruppe_id=request.user.gruppe.group_id).get(pk=prime.pk)
    except Books.DoesNotExist:
        return HttpResponseRedirect(reverse('webdms:index', args=[]))
    if request.POST:
        datap = request.POST.copy()
        datap['gruppe'] = request.user.gruppe.group_id
        bookbackuplist = Books.objects.filter(pk=datap['books'])
        backupbook = bookbackuplist.first()
        grupslug = slugify(request.user.gruppe.group.name)
        bckpord = slugify(backupbook.book) + '_' + datetime.now().strftime("__%Y-%m-%d_%H-%M-%S")
        bckpdir = path.join(backupdir, grupslug, bckpord)
        if not path.exists(bckpdir):
            makedirs(bckpdir)
        # create XML
        save_bookdb(bookbackuplist, grupslug, bckpord)
        # backup files
        src = path.join(dmsroot, grupslug, slugify(backupbook.book))
        save_bookdir(src, bckpdir)
        # siebenzipname = path.join(bckpdir + '.7z')
        # return sendfile(request, siebenzipname, True)
        return HttpResponseRedirect(reverse('webdms:bookrestore', args=[]))
    else:
        form = BooksForm(instance=book)
    if not path.exists(backupdir):
        makedirs(backupdir)
    backuplist = [f for f in listdir(backupdir) if isfile(join(backupdir, f))]
    backuplist.sort()  # list with files in backup directory
    dictionary = {
        'form': form,
        'booklist': booklist,
        'book': prime,  # 'primebook': prime.book,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'backuplist': backuplist,
        'pathlist': pathlist,
    }
    return render(request, 'books_backup.html', dictionary)


@login_required
def book_restore(request):
    """
    Buch auch Backup einlesen
    :param request:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    grupslug = slugify(request.user.gruppe.group.name)
    if request.POST:
        datap = request.POST.copy()
        datap['gruppe'] = request.user.gruppe.group_id
        if 'command' in datap:
            if datap['command'] == 'Datei laden':
                # load Datei
                form = UploadFileFormWeb7z(datap, request.FILES)
                if form.is_valid():
                    # nur Datei nach backupdir grupslug kopieren
                    siebenzdatei = handle_uploaded_media(request.FILES['file'], request.user, 'booksimport')
                    zieldatei = path.join(backupdir, grupslug, request.FILES['file'].name)
                    move(siebenzdatei, zieldatei)
                    # try:
                    #     with transaction.atomic():
                    #         BooksTemp.objects.filter(
                    #             gruppe_id=request.user.gruppe.group_id).delete()  # alte Überbleibssel löschen
                    #         xmldatei = handle_uploaded_media(request.FILES['file'], request.user, 'booksimport')
                    #         udata = try_utf8(xmldatei)
                    #         if not udata:
                    #             with open(xmldatei, encoding='latin-1') as f:
                    #                 data_ansi = f.read()
                    #             with open(xmldatei, 'w', encoding='utf8') as f:
                    #                 f.write(data_ansi)
                    #         # XML in BooksTemp einlesen + Anzeige der Books mit Optionen zum Ersetzen, Zusammen führen
                    #         # resultat = books_import_xml(xmldatei, request.user.gruppe.group_id)
                    #         books_import_xml(xmldatei, request.user.gruppe.group_id)
                    #         # if resultat:
                    #         #     print('Fehler')
                    #         # else:
                    #         #     print('Import Ok')
                    #         return HttpResponseRedirect(reverse('webdms:bookviewtemp', args=[]))
                    # except IntegrityError:
                    #     pass
            elif datap['command'] == 'Restore Starten':
                # Speicher Daten aus BooksTemp
                bookrestorenewlist = []
                for wert in datap:
                    if 'books-neu__' in wert:
                        bookrestorenewlist.append(datap[wert])
                books_import_temp(request.user.gruppe.group_id, bookrestorenewlist, 'neu')
                # bookrestorevorhandenlist = []
                bookrestoreersetzlist = []
                bookrestorezusammenlist = []
                for wert in datap:
                    if 'books-vorhanden__' in wert:
                        valuuid = datap[wert][4:]
                        varez = datap[wert][:1]
                        if varez == 'E':
                            bookrestoreersetzlist.append(valuuid)
                        elif varez == 'Z':
                            bookrestorezusammenlist.append(valuuid)
                        else:
                            continue
                # Import
                books_import_temp(request.user.gruppe.group_id, bookrestoreersetzlist, 'delete')
                books_import_temp(request.user.gruppe.group_id, bookrestoreersetzlist, 'neu')
                books_import_temp(request.user.gruppe.group_id, bookrestorezusammenlist, 'zusammen')
                # Import fertig --- hier BooksTemp löschen
                BooksTemp.objects.filter(gruppe_id=request.user.gruppe.group_id).delete()
                return HttpResponseRedirect(reverse('webdms:bookview', args=[]))
        # Falls ein anderer POST geschickt wird // Sollte nicht vorkommen
        form = UploadFileFormWeb7z()
    else:
        form = UploadFileFormWeb7z()
    # restorelist = booklist
    # Dateien in media backup
    verzeichnis = path.join(backupdir, grupslug)
    if not path.exists(verzeichnis):
        makedirs(verzeichnis)
    backuplist = [f for f in listdir(verzeichnis) if path.isfile(path.join(verzeichnis, f))]
    backuplist.sort()
    dictionary = {
        'form': form,
        'booklist': booklist,
        'book': prime,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'pathlist': pathlist,
        'backuplist': backuplist,
    }
    return render(request, 'books_restore.html', dictionary)


@login_required
def book_media(request, todo, dateiname):
    """
    shows media / backup directory (uploaded or stored backups)
    :param request:
    :param todo: 'loeschen' 'download' 'restore'
    :param dateiname:
    :return:
    """
    grupslug = slugify(request.user.gruppe.group.name)
    if todo == 'download':
        siebenzipname = path.join(backupdir, grupslug, dateiname)
        return sendfile(request, siebenzipname, True)
    elif todo == 'loeschen':
        siebenzipname = path.join(backupdir, grupslug, dateiname)
        if path.exists(siebenzipname):
            remove(siebenzipname)
    elif todo == 'restore':
        print('restore ' + dateiname)
        siebenzipname = path.join(backupdir, grupslug, dateiname)
        # py7zr
        odir = path.join(backupdir, grupslug)
        unpack_7zarchive(siebenzipname, odir)
        # Datei entpacken ok == ResourceWarning: unclosed file
        # xml import und verschieben
        # da in der xml das directory enthalten ist, ist die Prüfung einfacher
        # filename = dateiname[:-3]
        xmldateiname = path.join(odir, dateiname[:-3], dateiname[:-3] + '.xml')
        grupid = request.user.gruppe.group_id
        # try:
        #     with transaction.atomic():
        erg, dicname = books_import_xml(xmldateiname, grupid, grupslug)
        # except IntegrityError:
        #     pass
        # subdir duisburg fehlte unter test2
        ziel = path.join(dmsroot, grupslug)
        if not path.exists(ziel):
            makedirs(ziel)
        quelle = path.join(odir, dateiname[:-3])
        if erg == 0:
            move(quelle, ziel)
            zielname = path.join(dmsroot, grupslug, dateiname[:-3])
            origname = path.join(ziel, dicname)
            rename(zielname, origname)
        else:
            # remove uncompressed directory
            rmtree(siebenzipname[:-3])
        if erg > 0:
            return HttpResponseRedirect(reverse('webdms:treeerror', args=[erg]))
        # prüfen ob es das Verzeichnis schon gibt
    else:
        print('unbekannte funktion' + dateiname)
    return HttpResponseRedirect(reverse('webdms:bookview', args=[]))
