from .models import Url  # , Group
from .forms import UrlForm
from .tools import booknavi, pfadnavi, changeurl_ebene0, changeurl_ebene1, changeurl_ebene2
from django.conf import settings
from django.contrib.auth.decorators import login_required
# from django.db import IntegrityError  # transaction,
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.datastructures import MultiValueDictKeyError
from django.utils.text import slugify
from os import path, makedirs, rename, rmdir, listdir

mediadir = path.join(settings.MEDIA_ROOT)


@login_required
def tree_error(request, error):
    """
    Fehler anzeigen beim Anlegen eines tree
    :param request:
    :param error:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    errshowmenu = False
    if error == 1:
        msg = 'In der aktuellen Version können nur zwei Untermenü Ebenen angelegt werden'
        cardheader = 'Fehler beim Tree anlegen'
        errshowmenu = True
    elif error == 11:
        msg = 'Das Verzeichnis enthält noch Dateien und wurde deshalb nicht gelöscht.'
        cardheader = 'Fehler beim Tree löschen'
        errshowmenu = True
    elif error == 12:
        msg = 'Das Book enthält noch Dateien und Verzeichnisse und kann deshalb nicht gelöscht werden.'
        cardheader = 'Fehler beim Book löschen'
    elif error == 20:
        msg = 'Es gibt Dateien oder Unterverzeichnisse. Das Menü kann deshalb nicht geändert werden.'
        cardheader = 'Ändern nicht erlaubt'
    elif error == 21:
        msg = 'Den gewählten Eintrag gibt es nicht in der Tabelle Tree.'
        cardheader = 'Unbekannter Eintrag'
    elif error == 22:
        msg = 'Nicht unterstütztes Importformat.'
        cardheader = 'Restore abgebrochen'
    elif error == 23:
        msg = 'Es gibt bereits ein Book mit dem Namen. Restore nicht durchgeführt.'
        cardheader = 'Restore abgebrochen'
    else:
        msg = 'Unbekanter Fehler'
        cardheader = 'Unbekannter Fehler'
    dictionary = {
        'book': prime,
        'booklist': booklist,
        'pathlist': pathlist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'msg': msg,
        'cardheader': cardheader,
        'errshowmenu': errshowmenu,
    }
    return render(request, 'treeerror.html', dictionary)


@login_required
def menu_add(request, pathslug):
    """
    Um im Treemenü etwas anzulegen
    :param request:
    :param pathslug: URL die im Form direkt geladen werden soll
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    # select = prime.slug
    # level = 0
    if request.method == 'POST':
        datap = request.POST.copy()
        datap['book'] = prime.pk
        if 'direct' in datap:
            # nur Hauptgruppe ist zulässig
            datap['parent'] = prime.book
        datap['slug'] = slugify(datap['child'])  # Prüfen ob das im model gemacht wird
        try:
            wurzel = Url.objects.get(pk=datap['wurzel'])
            if wurzel.ebene > 1:
                return HttpResponseRedirect(reverse('webdms:treeerror', args=['1']))
            # if wurzel.parent == '':
            #     datap['parent'] = wurzel.child
            #     # level = 1
            datap['parent'] = wurzel.pfad
            level = wurzel.ebene + 1
        except Url.DoesNotExist:
            ###
            datap['parent'] = prime.book
            level = 0
            wurzel = Url.objects.none()
        # create folder
        wslug = slugify(datap['slug'])
        wparent = slugify(datap['parent'])
        wroot = datap['wurzel']
        # create_folders(level, prime, wroot, wparent, wslug, wurzel)
        grupslug = slugify(request.user.gruppe.group.name)
        filesubpfad = create_folders(level, prime, grupslug, wparent, wslug, wurzel)
        # datap['auf'] = 1  # Aufgeklappt
        datap['ebene'] = level
        # datap['pfad'] = slugify(datap['child'])
        # datap['teilpfad'] = path.join(prime.directory, slugify(datap['child']))

        try:
            parent = Url.objects.filter(book=prime).get(pfad=datap['parent'])
            pfad = parent.pfad + '/' + datap['child']
            parpath = parent.pfad
            pathsl = parent.slug + '.' + slugify(datap['child'])
            teilpfad = path.join(prime.directory, filesubpfad)  # parent.slug, slugify(datap['child']))
            try:
                wert = Url.objects.filter(book=prime).get(slug=parent.slug).auf
            except Url.DoesNotExist:
                wert = 1
        except Url.DoesNotExist:
            pfad = datap['child']
            pathsl = slugify(datap['child'])
            parpath = ''  # oder book verwenden
            wert = 1
            teilpfad = path.join(prime.directory, slugify(datap['child']))
        # sind in obj different
        datap['pfad'] = pfad  # slugify(datap['child'])
        datap['slug'] = pathsl
        datap['filesubpath'] = teilpfad
        datap['auf'] = wert
        datap['parent'] = parpath
        form = UrlForm(data=datap)  # (request.POST, instance=note)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.owner = request.user
            # obj.teilpfad = path.join(prime.directory, slugify(datap['child']))
            # obj.pfad = pfad
            # obj.parent = parpath
            # obj.slug = pathsl
            # obj.filesubpath = teilpfad
            # obj.auf = wert
            obj.save()
            # Hier sollte man auch die Tabelle Url füllen
            #
            # try:
            #     parent = Url.objects.filter(book=prime).get(pfad=datap['parent'])
            #     pfad = parent.pfad + '/' + datap['child']
            #     parpath = parent.pfad
            #     pathsl = parent.slug + '.' + slugify(datap['child'])
            #     teilpfad = path.join(prime.directory, parent.slug, slugify(datap['child']))
            #     try:
            #         wert = Url.objects.filter(book=prime).get(slug=parent.slug).auf
            #     except Url.DoesNotExist:
            #         wert = 1
            # except Url.DoesNotExist:
            #     pfad = datap['child']
            #     pathsl = slugify(datap['child'])
            #     parpath = ''  # oder book verwenden
            #     wert = 1
            #     teilpfad = path.join(prime.directory, slugify(datap['child']))
            # try:
            #     wert = Url.objects.filter(book=prime).get(slug=parent.slug).auf
            # except Url.DoesNotExist:
            #     wert = 1
            # Url.objects.create(book=prime,
            #                    slug=pathsl,
            #                    pfad=pfad,
            #                    parent=parpath,
            #                    ebene=level,
            #                    auf=wert,
            #                    filesubpath=teilpfad)
            # except:
            #     pass
    else:
        form = UrlForm(instance=None)
    # list0, list1, list2 = treecombo(prime, True)
    urllist = Url.objects.filter(book=prime)
    dictionary = {
        'book': prime,
        'booklist': booklist,
        'pathlist': pathlist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'form': form,
        'angemeldet': True,
        'error': False,
        'add': True,
        'titel': 'Tree anlegen',
        'select': pathslug,  # select,
        'urllist': urllist,
        'pathslug': pathslug,
    }
    return render(request, 'tree_main-page.html', dictionary)


@login_required
def url_add(request):
    """
    Um im Treemenü etwas anzulegen
    :param request:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    select = prime.slug
    # level = 0
    if request.method == 'POST':
        datap = request.POST.copy()
        datap['book'] = prime.pk
        datap['slug'] = slugify(datap['child'])  # Prüfen ob das im model gemacht wird
        try:
            wurzel = Url.objects.get(pk=datap['wurzel'])
            if wurzel.ebene > 1:
                return HttpResponseRedirect(reverse('webdms:treeerror', args=['1']))
            # if wurzel.parent == '':
            #     datap['parent'] = wurzel.child
            #     # level = 1
            datap['parent'] = wurzel.pfad
            level = wurzel.ebene + 1
            wslug = slugify(datap['slug'])
            wparent = slugify(datap['parent'])
            # wroot = datap['wurzel']
            datap['slug'] = wparent + '.' + wslug
        except Url.DoesNotExist:
            ###
            datap['parent'] = ''  # prime.book
            level = 0
            wurzel = Url.objects.none()
            wslug = slugify(datap['slug'])
            wparent = ''  # slugify(datap['parent'])
            # wroot = datap['wurzel']
        grupslug = slugify(request.user.gruppe.group.name)
        filesubpfad = create_folders(level, prime, grupslug, wparent, wslug, wurzel)
        datap['auf'] = 1  # Aufgeklappt
        datap['ebene'] = level
        try:
            datap['pfad'] = wurzel.pfad + '/' + datap['child']
            # bwurzel = True
        except AttributeError:
            datap['pfad'] = datap['child']
            # bwurzel = False
        form = UrlForm(data=datap)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.owner = request.user
            obj.filesubpath = path.join(prime.directory, filesubpfad)
            obj.save()
            # if bwurzel:
            #     return HttpResponseRedirect(reverse('webdms:menu_add', args=[wurzel.slug]))
            # else:
            #     return HttpResponseRedirect(reverse('webdms:treeadd', args=[]))
            return HttpResponseRedirect(reverse('webdms:menu_add', args=[obj.slug]))
    else:
        form = UrlForm(instance=None)
    urllist = Url.objects.filter(book=prime)
    dictionary = {
        'book': prime,
        'booklist': booklist,
        'pathlist': pathlist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'form': form,
        'angemeldet': True,
        'error': False,
        'add': True,
        'titel': 'Tree anlegen',
        'select': select,
        'urllist': urllist,
    }
    return render(request, 'tree_main-page.html', dictionary)


def create_folders(level, prime, grupslug, wparent, wslug, wurzel):
    """

    :param level:
    :param prime:
    :param grupslug: Gruppe
    :param wparent: slugify(datap['parent'])
    :param wslug: slugify(datap['slug'])
    :param wurzel:
    :return:
    """
    # try:
    #     select = Url.objects.get(pk=wroot).slug
    # except Url.DoesNotExist:
    #     select = prime.slug
    #
    if prime.bookroot == '':
        bookroot = path.join(mediadir, prime.slug)
    else:
        bookroot = path.join(prime.bookroot, grupslug, prime.slug)
    # bookroot gibt die Wurzel ab wo da UVZ angelegt werden
    if level == 0:
        # unter dem Book erste ebene anlegen; später book root auslesen
        # level0 = wslug  #  slugify(datap['slug'])
        filesubpfad = wslug
        bookdir0 = path.join(bookroot, filesubpfad)
        if not path.exists(bookdir0):
            makedirs(bookdir0)
    elif level == 1:
        # level0 = wparent  # slugify(datap['parent'])
        # subneu = wslug  # slugify(datap['slug'])
        filesubpfad = path.join(wparent, wslug)
        # bookdir0 = path.join(bookroot, level0, subneu)
        bookdir0 = path.join(bookroot, filesubpfad)
        if not path.exists(bookdir0):
            makedirs(bookdir0)
    elif level == 2:
        # pass datap['parent']  ist 'Schüssel-001/Modern Art'  splitt bei /  und
        teile = str(wurzel.slug).split('.')
        higherlevels = path.join(teile[0], teile[1])  # zwischen bookroot u
        # subneu = wslug  # slugify(datap['slug'])  # level2 # Grün  -- neues Unterverzeichnis
        # bookdir0 = path.join(bookroot, higherlevels, subneu)
        filesubpfad = path.join(higherlevels, wslug)
        bookdir0 = path.join(bookroot, filesubpfad)
        if not path.exists(bookdir0):
            makedirs(bookdir0)
    else:
        # gibt es derzeit nicht
        filesubpfad = ''
        pass  # oder redirekt auf Fehler
    return filesubpfad


@login_required
def url_change(request):  # , treeslug):
    # def tree_change(request, treeid):
    """
    Vorhandenen Tree ändern oder auch löschen
    :param request:
    param treeslug:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    # tabelleurl()
    pathurl = None  # tree = None
    changepost = False
    deletepost = False
    if request.POST:
        datap = request.POST.copy()
        datap['book'] = prime.pk
        try:
            treeid = datap['wurzel']  # request.POST['wurzel']
        except MultiValueDictKeyError:
            return HttpResponseRedirect(reverse('webdms:treeadd', args=[]))
        # try:
        #     tree = Trees.objects.get(pk=treeid)
        # except Trees.DoesNotExist:
        pathurl = Url.objects.get(pk=treeid)
        datap['slug'] = pathurl.slug
        # datap['direct'] = tree.direct
        datap['parent'] = pathurl.parent
        if request.POST['treechange'] == 'Remove Tree':
            deletepost = True
            # damit erst eine Anzeige zum löschen erscheint
            if 'pfad' in datap:
                datap['auf'] = pathurl.auf
                datap['ebene'] = pathurl.ebene
                datap['pfad'] = pathurl.pfad
        else:
            changepost = True
            # damit erst eine Anzeige zum bearbeiten mit dem Form-Feld erscheint
            if 'pfad' in datap:
                datap['auf'] = pathurl.auf
                datap['ebene'] = pathurl.ebene
                if pathurl.parent:
                    datap['slug'] = slugify(pathurl.parent) + '.' + slugify(datap['pfad'])
                    datap['pfad'] = pathurl.parent + '/' + datap['pfad']
                else:
                    datap['slug'] = slugify(datap['pfad'])
                    # datap['parent'] = ''  # Ist schon in datap
        form = UrlForm(instance=pathurl, data=datap)  # (request.POST, instance=note)
        if form.is_valid():
            grupslug = slugify(request.user.gruppe.group.name)
            dmsroot = pathurl.book.bookroot  # '~/git/dmsdir0/DEVELOP/dms'
            removedir = path.join(dmsroot, grupslug, pathurl.filesubpath)
            if deletepost:
                if pathurl.ebene == 0:
                    try:
                        if len(listdir(removedir)) == 0:
                            rmdir(removedir)
                            Url.objects.filter(book=pathurl.book).filter(ebene=0).filter(pk=pathurl.pk).delete()
                            # recursive remove not possible from listdir
                            # for ebene1 in Url.objects.filter(book=pathurl.book).filter(ebene=1).filter(parent=pathurl.pfad):
                            #     # Ebene2 Löschen
                            #     Url.objects.filter(book=pathurl.book).filter(ebene=2).filter(parent=ebene1.pfad).delete()
                            #     # Ebene 1 löschen
                            #     ebene1.delete()
                        else:
                            return HttpResponseRedirect(reverse('webdms:treeerror', args=[11]))
                    except FileNotFoundError:
                        # Verzeichnis existiert nicht mehr
                        Url.objects.filter(book=pathurl.book).filter(ebene=0).filter(pk=pathurl.pk).delete()
                        return HttpResponseRedirect(reverse('webdms:index', args=[]))
                elif pathurl.ebene == 1:
                    # Ebene2 Löschen
                    # Prüfen ob Verzeichnis leer ist
                    # removedir = path.join(dmsroot, grupslug, pathurl.filesubpath)
                    try:
                        if len(listdir(removedir)) == 0:
                            rmdir(removedir)
                            Url.objects.filter(book=pathurl.book).filter(ebene=2).filter(parent=pathurl.pfad).delete()
                        else:
                            return HttpResponseRedirect(reverse('webdms:treeerror', args=[11]))
                    except FileNotFoundError:
                        Url.objects.filter(book=pathurl.book).filter(ebene=1).filter(pk=pathurl.pk).delete()
                # ausgewählter Datensatz löschen
                # Prüfen ob Verzeichnis leer ist
                # removedir = path.join(dmsroot, grupslug, pathurl.filesubpath)
                try:
                    if len(listdir(removedir)) == 0:
                        rmdir(removedir)
                        pathurl.delete()
                    else:
                        return HttpResponseRedirect(reverse('webdms:treeerror', args=[11]))
                except FileNotFoundError:
                    pass
                return HttpResponseRedirect(reverse('webdms:treechange', args=[]))
            else:
                # Wenn es Unterverzeichnisse oder Dateien darin gibt, ist ändern nicht erlaubt
                # if not path.exists(removedir):  # wenn das Verzeichnis schon gelöscht ist
                if len(listdir(removedir)) == 0:
                    obj = form.save(commit=False)
                    dmsroot = obj.book.bookroot  # '~/git/dmsdir0/DEVELOP/dms'
                    oldpath = path.join(dmsroot, grupslug, obj.filesubpath)  # 'krimi/tv/schimanski'
                    newmenu = path.join(path.split(oldpath)[0], slugify(request.POST['pfad']))
                    # newbook = path.join(dmsroot, grupslug, str(obj.book).lower())
                    if path.exists(oldpath):
                        rename(oldpath, newmenu)
                    obj.owner = request.user
                    obj.save()
                    # ######  damit URL angepasst wird
                    if 'pfad' in form.initial:
                        oldpath = form.initial['pfad']
                        # oldparent = form.initial['parent']
                        urlpath = datap['pfad']
                        urlslug = datap['slug']
                        if obj.ebene == 0:
                            changeurl_ebene0(prime, oldpath, urlpath, urlslug)
                        elif obj.ebene == 1:
                            changeurl_ebene1(obj.pk, oldpath)
                        elif obj.ebene == 2:
                            changeurl_ebene2(obj, oldpath)
                    #######
                    return HttpResponseRedirect(reverse('webdms:treechange', args=[]))
                return HttpResponseRedirect(reverse('webdms:treeerror', args=[20]))
    else:
        form = UrlForm(instance=pathurl)
    urllist = Url.objects.filter(book=prime)
    # tree.child enthält den Teil der geändert werden soll
    try:
        urlchild = str(pathurl.path).replace(pathurl.parent, '')
        urlchild = str(urlchild).replace('/', '')
    except AttributeError:
        urlchild = ''
    dictionary = {
        'book': prime,
        'booklist': booklist,
        'pathlist': pathlist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'form': form,
        'angemeldet': True,
        'error': False,
        'add': False,
        'titel': 'Tree ändern',
        'tree': pathurl,  # tree,
        'changepost': changepost,
        'deletepost': deletepost,
        'urllist': urllist,
        'urlchild': urlchild,
    }
    return render(request, 'tree_main-page.html', dictionary)
