from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.db.models import Q
# from django.db import transaction, IntegrityError
from django.http import HttpResponseRedirect
from django.shortcuts import render  # , get_object_or_404
from django.urls import reverse
from django.utils.datastructures import MultiValueDictKeyError
from django.utils.text import slugify
# from dmsapp.views import file_import
from datetime import datetime
from webdms.models import Dateien, Url, Schluessel, Schluessel2Datei, Books
from webdms.pathurl import dateiurl, dateiurlempty  # belegurlempty belegurl,
from webdms.tools import booknavi, pfadnavi
from .form import DateiForm
from webdms.jptools.datei_helfer import handle_uploaded_media
from os import path, makedirs, rename, remove
from shutil import move

dmsroot = path.join(settings.SENDFILE_ROOT)


@login_required
def datei_add(request, pathslug):
    """
    Neue Notiz anlegen
    :param request:
    :param pathslug:
    :return:
    """
    try:
        urlpath = Url.objects.get(slug=pathslug)
        urlslug = urlpath.slug
    except Url.DoesNotExist:
        return HttpResponseRedirect(reverse('webdms:index', args=[]))
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    if request.method == 'POST':
        datap = request.POST.copy()
        datap['book'] = prime.pk
        if datap['pfad'] == 'None':
            datap['pfad'] = None
        new_dateiname = ''
        if 'label' in datap:
            if str(datap['label']).strip() == '':
                try:
                    datap['label'] = request.FILES['file'].name
                except MultiValueDictKeyError:
                    return HttpResponseRedirect(reverse('webdms:notefehler', args=[pathslug, 3]))
            else:
                new_dateiname = datap['label']  # + '.ext'

            datap['slug'] = slugify(datap['label'])  # Prüfen ob das im model gemacht wird
        uploadeddatei = handle_uploaded_media(request.FILES['file'], request.user, '')
        datap['dmsname'] = path.basename(uploadeddatei)
        form = DateiForm(data=datap)  # (request.POST, instance=note)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.owner = request.user
            obj.save()
            # kwschluessel__
            for wert in datap:
                if 'kwschluessel__' in wert:
                    key = Schluessel.objects.get(pk=wert[14:])
                    keydt = Schluessel2Datei.objects.filter(schluessel=key).filter(datei=obj)  # datei)
                    if not keydt.exists():
                        Schluessel2Datei.objects.create(schluessel=key,
                                                        datei=obj)  # datei)
            # move uploadeddatei to ziel
            grupslug = slugify(request.user.gruppe.group.name)
            ziel = path.join(urlpath.book.bookroot, grupslug, urlpath.filesubpath)
            if not path.exists(ziel):
                makedirs(ziel)
            if path.isfile(uploadeddatei):
                if len(new_dateiname) > 0:
                    pfadsource, pfaddatei = path.split(uploadeddatei)
                    filename, file_extension = path.splitext(pfaddatei)
                    datum = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
                    if file_extension in new_dateiname:
                        neuername = datum + '__' + new_dateiname
                    else:
                        neuername = datum + '__' + new_dateiname + file_extension
                    pfadziel = path.join(pfadsource, neuername)
                    rename(uploadeddatei, pfadziel)
                else:
                    pfadziel = uploadeddatei
                move(pfadziel, ziel)
            return HttpResponseRedirect(reverse('webdms:path:view', args=[pathslug]))
        else:
            if Dateien.objects.filter(book=prime.pk).filter(pfad=urlpath).filter(label=datap['label']).exists():
                return HttpResponseRedirect(reverse('webdms:notefehler', args=[pathslug, 2]))  # prüfen
    else:
        form = DateiForm(instance=None)
    notelist = dateiurl(prime, pathslug)
    dateilist = dateiurl(prime, urlslug)
    urlaction = reverse('webdms:path:add', args=[pathslug])
    dictionary = {
        'form': form,
        'booklist': booklist,
        'book': prime,
        'dateilist': dateilist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'notelist': notelist,
        'angemeldet': True,
        'error': False,
        'pathslug': pathslug,
        'urlid': urlpath.pk,
        'pathlist': pathlist,
        'addnotiz': True,
        'urllist': pathlist,  # ist hier kürzer als die komplette URL liste
        'urlaction': urlaction,
    }
    return render(request, 'wspath/datei_add_form.html', dictionary)


@login_required
def datei_edit(request, pathslug):
    try:
        urlpath = Url.objects.get(slug=pathslug)
    except Url.DoesNotExist:
        return HttpResponseRedirect(reverse('webdms:index', args=[]))
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    if request.method == 'POST':
        datap = request.POST.copy()
        datap['book'] = prime.pk
        if datap['pfad'] == 'None':
            datap['pfad'] = None
        if datap['keyword'] == 'None':
            datap['keyword'] = None
        new_dateiname = ''
        datap['hinweis'] = ''
        if 'label' in datap:
            if str(datap['label']).strip() == '':
                datap['label'] = request.FILES['file'].name
            else:
                new_dateiname = datap['label']  # + '.ext'

        datap['slug'] = slugify(datap['label'])  # Prüfen ob das im model gemacht wird
        form = DateiForm(data=datap)  # (request.POST, instance=note)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.owner = request.user
            obj.save()
            # Datei in path.join(urlpath.book.bookroot, urlpath.book.directory, urlpath.Filesubpath) ablegen
            uploadeddatei = handle_uploaded_media(request.FILES['file'], request.user, '')
            # move uploadeddatei to ziel
            ziel = path.join(urlpath.book.bookroot, urlpath.book.directory, urlpath.filesubpath)
            if not path.exists(ziel):
                makedirs(ziel)
            if path.isfile(uploadeddatei):
                if len(new_dateiname) > 0:
                    pfadsource, pfaddatei = path.split(uploadeddatei)
                    filename, file_extension = path.splitext(pfaddatei)
                    datum = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
                    if file_extension in new_dateiname:
                        neuername = datum + '__' + new_dateiname
                    else:
                        neuername = datum + '__' + new_dateiname + file_extension
                    pfadziel = path.join(pfadsource, neuername)
                    rename(uploadeddatei, pfadziel)
                else:
                    pfadziel = uploadeddatei
                move(pfadziel, ziel)
            return HttpResponseRedirect(reverse('webdms:path:view', args=[pathslug]))
        else:
            if Dateien.objects.filter(book=prime.pk).filter(pfad=urlpath).filter(label=datap['label']).exists():
                return HttpResponseRedirect(reverse('webdms:notefehler', args=[pathslug, 2]))  # prüfen
    else:
        form = DateiForm(instance=None)
    notelist = dateiurl(prime, pathslug)
    urlaction = reverse('webdms:path:edit', args=[pathslug])
    dictionary = {
        'form': form,
        'booklist': booklist,
        'book': prime,  # 'primebook': prime.book,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'notelist': notelist,
        'angemeldet': True,
        'error': False,
        'pathslug': pathslug,
        'urlid': urlpath.pk,
        'pathlist': pathlist,
        # 'addnotiz': True,
        'urllist': pathlist,  # ist hier kürzer als die komplette URL liste
        'urlaction': urlaction,
    }
    return render(request, 'wspath/datei_add_form.html', dictionary)


@login_required
def path_alle(request):
    """
    Alle Notizen anzeigen
    :param request:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    dateilist = Dateien.objects.filter(book=prime)
    dictionary = {
        'booklist': booklist,
        'book': prime,
        'dateilist': dateilist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'pathslug': None,  # pathslug,
        'pathlist': pathlist,
        # 'beleglist': beleglist,
    }
    return render(request, 'wspath/path_empty.html', dictionary)


@login_required
def path_delete(request, dateislug):
    """
    Vorhandene Notiz ändern
    :param request:
    :param dateislug:
    :return:
    """
    try:
        datei = Dateien.objects.get(slug=dateislug)
    except Dateien.DoesNotExist:
        # gibt es nicht
        return HttpResponseRedirect(reverse('webdms:index', args=[]))
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    if datei.pfad is None:
        pathslug = None  # urlid =
        # notelist = emptylist
        dateilist = dateiurlempty(prime)
        # beleglist = belegurlempty(prime)
    else:
        pathslug = datei.pfad.slug
        # urlid = datei.pfad_id
        dateilist = dateiurl(prime, pathslug)
        # beleglist = belegurl(prime, pathslug)
    if request.method == 'POST':
        datap = request.POST.copy()
        datap['book'] = datei.book_id  # prime.pk
        datap['slug'] = datei.slug  # Prüfen ob das im model gemacht
        datap['dmsname'] = datei.dmsname
        # Damit Einträge "Ohne Pfad gelöscht werden können
        if datei.pfad is None:
            datap['pfad'] = None
        else:
            datap['pfad'] = datei.pfad.pk
        form = DateiForm(instance=datei, data=datap)  # (request.POST, instance=note)
        if form.is_valid():
            # path.join(urlpath.book.bookroot, grupslug, urlpath.filesubpath)
            if datei.pfad is not None:
                grupslug = slugify(request.user.gruppe.group.name)
                dmsrootbook = datei.book.bookroot  # '~/git/dmsdir0/DEVELOP/dms'
                rmvfile = path.join(dmsrootbook, grupslug, datei.pfad.filesubpath, datei.dmsname)
                if path.exists(rmvfile):
                    # hier erst Datei löschen bevor der Eintrag aus der DB entfernt wird
                    remove(rmvfile)
                # remove entry, if file is not existent it is obsolete
            datei.delete()
        if pathslug is None:
            return HttpResponseRedirect(reverse('webdms:path:empty', args=[]))
        return HttpResponseRedirect(reverse('webdms:path:view', args=[pathslug]))
    else:
        form = DateiForm(instance=datei)
    dictionary = {
        'form': form,
        'booklist': booklist,
        'book': prime,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'dateilist': dateilist,
        'angemeldet': True,
        'error': False,
        'datei': datei,
        'loeschen': datei,
        'pathlist': pathlist,
        'pathslug': pathslug,
        # 'beleglist': beleglist,
    }
    return render(request, 'wspath/datei_add_form.html', dictionary)


@login_required
def path_empty(request):
    """
    Zur Anzeige von Notizen ohne Zuordnung
    :param request:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    notelist = dateiurlempty(prime)  # , pathslug)
    # beleglist = belegurlempty(prime)  # , pathslug)
    dictionary = {
        'booklist': booklist,
        'book': prime,
        'notelist': notelist,
        'emptylist': emptylist,
        'pathlist': pathlist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'pathslug': None,  # pathslug,
    }
    return render(request, 'wspath/path_empty.html', dictionary)


@login_required
def path_datei(request, pathslug, dateislug):
    """
    Notiz anzeigen ? -- optional ohne pathslug aufrufen
    :param request:
    :param pathslug:
    :param dateislug:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    try:
        urlpath = Url.objects.filter(book=prime).get(slug=pathslug)
    except Url.DoesNotExist:
        return HttpResponseRedirect(reverse('webdms:index', args=[]))
    if Books.objects.filter(is_active=True).filter(book=urlpath.book).count() == 0:
        # urlpath is in other book
        book = Books.objects.filter(gruppe_id=request.user.gruppe.group_id).get(pk=urlpath.book_id)
        book.is_active = True
        book.save()
    try:
        datei = Dateien.objects.filter(pfad__slug=pathslug).get(slug=dateislug)
        datei.is_active = True
        datei.save(update_fields=['is_active'])
    except Dateien.DoesNotExist:
        if pathslug == 'None':
            try:
                datei = Dateien.objects.filter(pfad__isnull=True).get(slug=dateislug)
            except Dateien.DoesNotExist:
                return HttpResponseRedirect(reverse('webdms:index', args=[]))
        else:
            return HttpResponseRedirect(reverse('webdms:index', args=[]))
    # schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    # pathlist, emptylist = pfadnavi(prime)
    dateilist = dateiurl(prime, pathslug)
    if request.method == 'POST':
        datap = request.POST.copy()
        datap['book'] = datei.book_id
        datap['slug'] = datei.slug
        datap['dmsname'] = datei.dmsname
        # kwschluessel__
        keyonlist = []
        for wert in datap:
            if 'kwschluessel__' in wert:
                key = Schluessel.objects.get(pk=wert[14:])
                keydt = Schluessel2Datei.objects.filter(schluessel=key).filter(datei=datei)
                if not keydt.exists():
                    Schluessel2Datei.objects.create(schluessel=key,
                                                    datei=datei)
                keyonlist.append(key)
        # remove other keys from datei
        Schluessel2Datei.objects.filter(datei=datei).exclude(schluessel__in=keyonlist).delete()
        form = DateiForm(instance=datei, data=datap)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.owner = request.user
            obj.save()
            # working, but it looks like nothing has changed
            # return HttpResponseRedirect(reverse('webdms:path:datei', args=[pathslug, obj.slug]))
            return HttpResponseRedirect(reverse('webdms:path:view', args=[pathslug]))
        else:
            if Dateien.objects.filter(book=prime.pk).filter(pfad=urlpath).filter(label=datap['label']).exists():
                return HttpResponseRedirect(reverse('webdms:notefehler', args=[pathslug, 2]))  # prüfen
    else:
        form = DateiForm(instance=datei)
    urlaction = reverse('webdms:path:datei', args=[pathslug, dateislug])
    # muss noch auf Schlüssel in den eigenen Büchern oder Gruppen begrenzt werden
    usedschluessel2datei = Schluessel2Datei.objects.filter(datei=datei)
    usedschluessellist = Schluessel.objects.filter(schluessel2datei__in=usedschluessel2datei)
    dictionary = {
        'form': form,
        'booklist': booklist,
        'book': prime,
        'dateilist': dateilist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'pathslug': pathslug,
        'urlid': urlpath.pk,
        'pathlist': pathlist,
        'datei': datei,
        'urlslug': datei.pfad.slug,
        'addnotiz': True,  # zur Anzeige testen
        'urlaction': urlaction,
        'usedschluessellist': usedschluessellist,
    }
    return render(request, 'wspath/path.html', dictionary)


@login_required
def path_dateichange(request, dateislug):
    """
    Vorhandene Notiz ändern
    :param request:
    :param dateislug:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    urllist = Url.objects.filter(book__gruppe=prime.gruppe)
    try:
        datei = Dateien.objects.filter(book=prime).get(slug=dateislug)
    except Dateien.DoesNotExist:
        return HttpResponseRedirect(reverse('webdms:index', args=[]))
    if datei.pfad is None:
        pathslug = urlid = None
        dateilist = dateiurlempty(prime)
    else:
        pathslug = datei.pfad.slug
        urlid = datei.pfad_id
        dateilist = dateiurl(prime, pathslug)
    if request.method == 'POST':
        datap = request.POST.copy()
        if datap['pfad'] == 'None':
            datap['pfad'] = None
            datap['book'] = datei.book_id
        else:
            try:
                datap['book'] = Url.objects.get(pk=datap['pfad']).book_id
            except Url.DoesNotExist:
                datap['book'] = datei.book_id
        datap['slug'] = slugify(datap['label'])
        # for empty html field
        # if datap['content'] == '':
        #     datap['content'] = '<p>&nbsp;</p>'
        form = DateiForm(instance=datei, data=datap)  # (request.POST, instance=note)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.owner = request.user
            obj.save()
        if datei.pfad is None:
            # pathslug = None
            return HttpResponseRedirect(reverse('webdms:path:noteempty', args=[datei.slug]))
        elif pathslug != datei.pfad.slug:
            if dateilist.count() > 0:
                dtfirst = dateilist.first()
                return HttpResponseRedirect(reverse('webdms:path:datei', args=[pathslug, dtfirst.slug]))
            else:
                return HttpResponseRedirect(reverse('webdms:path:view', args=[pathslug]))
        else:
            return HttpResponseRedirect(reverse('webdms:path:datei', args=[pathslug, datei.slug]))
    else:
        form = DateiForm(instance=datei)
    usedschluessel2datei = Schluessel2Datei.objects.filter(datei=datei)
    usedschluessellist = Schluessel.objects.filter(schluessel2datei__in=usedschluessel2datei)
    dictionary = {
        'form': form,
        'datei': datei,
        'booklist': booklist,
        'book': prime,
        'dateilist': dateilist,
        # 'beleglist': beleglist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'addnotiz': True,
        # 'keyid': datei.keyword_id,
        'pathslug': pathslug,
        'pathlist': pathlist,
        'urlid': urlid,
        'urllist': urllist,
        'usedschluessellist': usedschluessellist,
    }
    return render(request, 'wspath/datei_add_form.html', dictionary)


@login_required
def path_dateiempty(request, dateislug):
    """
    Notiz anzeigen ? -- optional ohne pathslug aufrufen
    :param request:
    :param dateislug:
    :return:
    """
    try:
        datei = Dateien.objects.filter(pfad__isnull=True).get(slug=dateislug)
    except Dateien.DoesNotExist:
        return HttpResponseRedirect(reverse('webdms:index', args=[]))
    if request.method == 'POST':
        datap = request.POST.copy()
        # check ob Datei sich an dieser Stelle befindet
        try:
            urlpfad = Url.objects.get(pk=datap['pfad'])
            pfadslug = urlpfad.slug
            newfilesubname = urlpfad.filesubpath
        except Url.DoesNotExist:
            return HttpResponseRedirect(reverse('webdms:treeerror', args=[21]))
        grupslug = slugify(request.user.gruppe.group.name)
        dateipfad = path.join(dmsroot, grupslug, newfilesubname, datei.dmsname)
        if not path.exists(dateipfad):
            return HttpResponseRedirect(reverse('webdms:notefehler', args=[pfadslug, 4]))
        datap['dmsname'] = datei.dmsname
        datap['book'] = datei.book_id
        datap['slug'] = datei.slug
        form = DateiForm(instance=datei, data=datap)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.owner = request.user
            obj.save()
    else:
        form = DateiForm(instance=datei)
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    dateilist = dateiurlempty(prime)
    dictionary = {
        'form': form,
        'booklist': booklist,
        'book': prime,
        'dateilist': dateilist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'pathslug': None,
        'pathlist': pathlist,
        'datei': datei,
        'addnotiz': True,
    }
    return render(request, 'wspath/path_empty.html', dictionary)


@login_required
def path_view(request, pathslug):  # , childslug, pathslug):  #
    # def child_view(request, childid):
    """
    Unter dem Hauptstamm kommt ert ein Untermenü und dann die Notizen
    Notizen (Dokumente) unterhalb des Path anzeigen
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    try:
        # childid = Trees.objects.get(slug=childslug).pk
        urlslug = Url.objects.filter(book=prime).get(slug=pathslug).slug
    except Url.DoesNotExist:
        return HttpResponseRedirect(reverse('webdms:index', args=[]))
    # schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    # pathlist, emptylist = pfadnavi(prime)
    dateilist = dateiurl(prime, urlslug)
    dictionary = {
        'booklist': booklist,
        'book': prime,  # 'primebook': prime.book,
        'dateilist': dateilist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'pathslug': pathslug,
        'pathlist': pathlist,
        'urlslug': urlslug,
    }
    return render(request, 'wspath/path.html', dictionary)


@login_required
def path_tree(request, pathslug):
    """
    Ändert den Wert auf in der Tabelle URL was die Anzeige des Tress ändert
    :param request:
    :param pathslug:
    :return:
    """
    try:
        url = request.META['HTTP_REFERER']
    except KeyError:
        url = reverse('webdms:index', args=[])
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    try:
        par = Url.objects.filter(book=prime).filter(ebene=0).get(slug=pathslug)
        if par.auf == 1:
            wert = 0
            # Zweite Ebene muss auch zugeklappt werden
            urlpath = Url.objects.filter(book=prime).filter(parent=par.pfad)
            for urp in urlpath:
                Url.objects.filter(book=prime).filter(parent=urp.pfad).update(auf=wert)
            urlpath.update(auf=wert)
        else:
            wert = 1
            # nur das erste Untermenü aufklappen
            Url.objects.filter(book=prime).filter(parent=par.pfad).update(auf=wert)
        par.auf = wert
        par.save()
    except Url.DoesNotExist:
        par = Url.objects.filter(book=prime).filter(ebene=1).get(slug=pathslug)
        if Url.objects.filter(book=prime).filter(parent=par.pfad).first().auf == 1:
            wert = 0
        else:
            wert = 1
        Url.objects.filter(book=prime).filter(parent=par.pfad).update(auf=wert)
    except Url.DoesNotExist:
        # Derzeit nur 2 Ebenen unterstützt
        pass
    return HttpResponseRedirect(url)


@login_required
def searchsl_view(request, pathslug):
    """
    Suche mit pathslug damit das Menü aktiv bleibt
    :param request:
    :param pathslug:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    # https://www.calazan.com/adding-basic-search-to-your-django-site/
    dateilist = []
    allebooks = btree = btitel = False
    suchenach = ''
    if request.method == 'GET':  # 'POST':
        datag = request.GET.copy()
        if 'search' in datag:
            suchenach = datag['search']
            # Um in allen Büchern zu suchen
            if 'allebooks' in datag:
                allebooks = datag['allebooks']
            # in Titel suchen
            btitel = False
            try:
                if str(datag['titel']) == 'on':
                    btitel = True
            except MultiValueDictKeyError:
                btitel = False
            # denkbar wäre auch im Tree zu suchen
            # es werden nur Notizen gefunden
            btree = False
            try:
                if str(datag['stree']) == 'on':
                    btree = True
            except MultiValueDictKeyError:
                btree = False
            if allebooks:
                if btitel and btree:
                    dateilist = Dateien.objects.filter(book__in=booklist).filter(
                        Q(label__icontains=suchenach) | Q(pfad__pfad__icontains=suchenach) | Q(
                            hinweis__icontains=suchenach))
                elif btitel:
                    dateilist = Dateien.objects.filter(book__in=booklist).filter(
                        Q(label__icontains=suchenach) | Q(hinweis__icontains=suchenach))
                elif btree:
                    dateilist = Dateien.objects.filter(book__in=booklist).filter(
                        Q(pfad__pfad__icontains=suchenach) | Q(hinweis__icontains=suchenach))
                else:
                    dateilist = Dateien.objects.filter(book__in=booklist).filter(hinweis__icontains=suchenach)
            else:
                if btitel and btree:
                    dateilist = Dateien.objects.filter(book=prime).filter(
                        Q(label__icontains=suchenach) | Q(pfad__pfad__icontains=suchenach) | Q(
                            hinweis__icontains=suchenach))
                elif btitel:
                    dateilist = Dateien.objects.filter(book=prime).filter(
                        Q(label__icontains=suchenach) | Q(hinweis__icontains=suchenach))
                elif btree:
                    dateilist = Dateien.objects.filter(book=prime).filter(
                        Q(pfad__pfad__icontains=suchenach) | Q(hinweis__icontains=suchenach))
                else:
                    dateilist = Dateien.objects.filter(book=prime).filter(hinweis__icontains=suchenach)
    actionurl = reverse('webdms:path:searchsl', args=[pathslug])
    dictionary = {
        'booklist': booklist,
        'book': prime,
        'dateilist': dateilist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,

        'angemeldet': True,
        'error': False,
        'add': True,
        'titel': 'Suchen in Notizen und Menü',
        'pathlist': pathlist,
        'pathslug': pathslug,
        'allebooks': allebooks,
        'btree': btree,
        'btitel': btitel,
        'suchtext': suchenach,
        'actionurl': actionurl,
    }
    return render(request, 'suche_main-page.html', dictionary)
