from django import forms
from webdms.models import Dateien
from django.utils.text import slugify
import itertools


class DateiForm(forms.ModelForm):
    # body = forms.CharField(widget=PagedownWidget)

    class Meta:
        model = Dateien
        # fields = ('label', 'body')
        exclude = ('date_created', 'date_updated')

    def __init__(self, **kwargs):
        # def __init__(self, *args, **kwargs):
        try:
            if Dateien.objects.filter(book=kwargs['data']['book']).filter(slug=kwargs['data']['slug']).exists():
                orig = kwslug = kwargs['data']['slug']
                for x in itertools.count(1):
                    if not Dateien.objects.filter(slug=kwslug).exists():
                        break
                    kwslug = '%s-%d' % (orig, x)
                kwargs['data']['slug'] = kwslug
        except KeyError:
            pass
        self.__user = kwargs.pop('user', None)
        super(DateiForm, self).__init__(**kwargs)

    def save(self, commit=True):
        if self.instance.pk is None:
            if self.__user is None:
                raise TypeError("You didn't give an user argument to the constructor.")
            self.instance.slug = slugify(self.instance.label)
            self.instance.author = self.__user
        return super(DateiForm, self).save(commit)


def validate_file_rtf(value):
    # if not value.name.lower().endswith('.rtf'):
    #     raise forms.ValidationError("Es können nur RTF-Dateien eingelesen werden.")
    # if not value.name.lower().endswith('.txt') and not value.name.lower().endswith(
    #         '.rtf') and not value.name.lower().endswith('.html'):
    if not value.name.lower().endswith('.txt') and not value.name.lower().endswith(
            '.html') and not value.name.lower().endswith('.htm'):
        raise forms.ValidationError("Es können nur TXT- und HTML-Dateien eingelesen werden. (RTF-Dateien können mit "
                                    "OpenOffice 'lowriter --headless --convert-to html datei.rtf' in html umgewandelt "
                                    "werden.)")


class UploadFileFormRTF(forms.Form):
    file = forms.FileField(label='Bitte eine RTF-Datei auswählen', validators=[validate_file_rtf])
