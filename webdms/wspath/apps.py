from django.apps import AppConfig


class WspathConfig(AppConfig):
    name = 'wspath'
