from django.urls import path
from .path import path_view, path_datei, path_dateichange, datei_add, path_empty, path_dateiempty, \
    path_delete, path_alle, path_tree, searchsl_view
app_name = 'notes_path'


# webdms:path:datei
urlpatterns = [
    # 'webdms/path/' ...
    path('add/<str:pathslug>/', datei_add, name='add'),
    path('alle/', path_alle, name='alle'),
    path('empty/', path_empty, name='empty'),
    path('datei/<str:pathslug>/<slug:dateislug>/', path_datei, name='datei'),
    path('view/<str:pathslug>/', path_view, name='view'),
    path('change/<slug:dateislug>/', path_dateichange, name='notechange'),
    path('delete/<slug:dateislug>/', path_delete, name='delete'),
    path('dateiempty/<slug:dateislug>/', path_dateiempty, name='dateiempty'),
    path('suchen/<str:pathslug>/', searchsl_view, name='searchsl'),
    path('tree/<str:pathslug>/', path_tree, name='treechange'),
]
