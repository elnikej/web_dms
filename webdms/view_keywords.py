from .models import Schluessel, Dateien, Books, Schluessel2Datei
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.text import slugify
from .forms import SchluesselForm
from .tools import booknavi, pfadnavi


@login_required
def schluessel_add(request):
    """
    KeyWords anlegen (in Liste)
    :param request:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    if request.POST:
        datap = request.POST.copy()
        datap['gruppe'] = request.user.gruppe.group_id
        datap['slug'] = slugify(datap['schluessel'])
        # Wenn es noch kein Buch gibt, muss
        if Books.objects.filter(gruppe_id=request.user.gruppe.group_id).count() == 0:
            # datap['is_active'] = True
            return HttpResponseRedirect(reverse('webdms:bookadd', args=[]))
        datap['book'] = prime.pk
        form = SchluesselForm(data=datap)  # (request.POST, instance=note)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.owner = request.user
            obj.save()
            return HttpResponseRedirect(reverse('webdms:keywords_view', args=[]))
    else:
        form = SchluesselForm(instance=None)
    dictionary = {
        'form': form,
        'booklist': booklist,
        'book': prime,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'addkeyword': True,
    }
    return render(request, 'schluessellist.html', dictionary)


@login_required
def schluessel_view(request):
    """
    Liste mit vorhandenen KeyWörtern anzeigen
    :param request:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    dictionary = {
        'booklist': booklist,
        'book': prime,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
    }
    return render(request, 'schluessellist.html', dictionary)


@login_required
def schluessel_edit(request, keyslug):
    """
    KeyWord ändern
    :param request:
    :param keyslug:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    try:
        objekt = Schluessel.objects.get(slug=keyslug)
    except Schluessel.DoesNotExist:
        return HttpResponseRedirect(reverse('webdms:keywords_view', args=[]))
    if request.POST:
        datap = request.POST.copy()
        datap['gruppe'] = request.user.gruppe.group_id
        datap['slug'] = slugify(datap['keyword'])
        # Wenn es noch kein Buch gibt, muss
        # if Books.objects.filter(gruppe_id=request.user.gruppe.group_id).count() == 0:
        #     # datap['is_active'] = True
        #     return HttpResponseRedirect(reverse('webdms:bookadd', args=[]))
        datap['book'] = prime.pk
        # form = KeyWordsForm(data=datap)  # (request.POST, instance=note)
        form = SchluesselForm(instance=objekt, data=datap)
        if form.is_valid():
            if datap['subkey'] == 'Remove KeyWord':
                objekt.delete()
            else:
                obj = form.save(commit=False)
                obj.owner = request.user
                obj.save()
            return HttpResponseRedirect(reverse('webdms:keywords_view', args=[]))
    else:
        form = SchluesselForm(instance=objekt)
    dictionary = {
        'form': form,
        'booklist': booklist,
        'book': prime,  # 'primebook': primebook,
        # 'treelist': treelist,
        # 'directlist': directlist,
        # 'parentlist': parentlist,
        # 'childlist': childlist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        # 'titel': 'Books',
        'addkeyword': True,
        # 'mygruppe': str(request.user.gruppe.group),
        'object': objekt,
    }
    return render(request, 'schluessellist.html', dictionary)


@login_required
def schluessel_find(request, keyslug):
    """
    Notes (label, tree und book) anzeigen mit Link um dahin zu springen
    evtl später auf Books filtern oder beschränken
    :param request:
    :param keyslug:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    # usedschluessel2datei = Schluessel2Datei.objects.filter(datei=datei)  # zum check wir Liste als Schluessel benötigt
    # usedschluessellist = Schluessel.objects.filter(schluessel2datei__in=usedschluessel2datei)
    keylist = Schluessel.objects.filter(slug=keyslug)
    try:
        keywordused = keylist.first()
    except Schluessel.DoesNotExist:
        keywordused = Schluessel.objects.none()
    # dateilist = Dateien.objects.filter(keyword__in=keylist)
    dateilist = Dateien.objects.filter(schluessel2datei__schluessel__in=keylist)
    if dateilist.count() == 1:
        # die erste Notiz direkt anzeigen, wenn es nur eine gibt
        datei = dateilist.first()
        book = Books.objects.filter(gruppe_id=request.user.gruppe.group_id).get(slug=datei.book.slug)
        book.is_active = True
        book.save()
        # if there is only one file show details
        return HttpResponseRedirect(reverse('webdms:keywordsviewnotes', args=[datei.book.slug,
                                                                              keyslug,  # keywordused.slug,  # datei.keyword.slug,
                                                                              datei.slug]))
    dictionary = {
        'booklist': booklist,
        'book': prime,
        'dateilist': dateilist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'keywordused': keywordused,
        'pathlist': pathlist,
    }
    return render(request, 'schluesselfound.html', dictionary)


@login_required
def schluessel_view_notes(request, bookslug, keyslug, dateislug):
    """
    Notiz anzeigen mit Keywordliste
    :param request:
    :param bookslug:
    # param keyslug: ist nicht mehr in Tabelle Schluessel
    :param dateislug:
    :return:
    """
    try:
        book = Books.objects.filter(gruppe_id=request.user.gruppe.group_id).get(slug=bookslug)
        if not book.is_active:
            book.is_active = True
            book.save()
    except Books.DoesNotExist:
        return HttpResponseRedirect(reverse('webdms:index', args=[]))
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    keylist = Schluessel.objects.filter(slug=keyslug)
    try:
        keywordused = keylist.first()
    except Schluessel.DoesNotExist:
        keywordused = Schluessel.objects.none()
    dateilist = Dateien.objects.filter(schluessel2datei__schluessel__in=keylist)
    try:
        objekt = Dateien.objects.filter(book=prime).get(slug=dateislug)
    except Dateien.DoesNotExist:
        objekt = Dateien.objects.none()
    usedschluessel2datei = Schluessel2Datei.objects.filter(datei=objekt)  # zum check wir Liste als Schluessel benötigt
    usedschluessellist = Schluessel.objects.filter(schluessel2datei__in=usedschluessel2datei)
    dictionary = {
        'booklist': booklist,
        'book': prime,
        'dateilist': dateilist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,
        'angemeldet': True,
        'error': False,
        'object': objekt,
        'keywordused': keywordused,
        'pathlist': pathlist,
        'usedschluessellist': usedschluessellist,
    }
    return render(request, 'schluesselnote.html', dictionary)
