from django.db import models
from django.contrib.auth.models import Group
# from django.contrib.auth.models import User, Permission  # für Lokale Userverwaltung
from django.utils.text import slugify
# from django.conf import settings
# from tinymce.models import HTMLField
from uuid import uuid4
import itertools


class Books(models.Model):
    """Book where the Files (Dateien) are in"""
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    book = models.CharField(max_length=32, db_index=True)
    slug = models.SlugField(max_length=36, null=True, blank=True)
    gruppe = models.ForeignKey(Group, on_delete=models.SET_NULL, null=True, db_index=True)
    is_active = models.BooleanField(default=False)
    bookroot = models.CharField(max_length=256, default='', blank=True)
    directory = models.CharField(max_length=126, default='', blank=True)
    datum_created = models.DateTimeField(auto_now_add=True, editable=False)
    datum_updated = models.DateTimeField(auto_now=True, editable=False)
    
    def __str__(self):
        return "Book: {0} / Gruppe: {1}".format(self.book, self.gruppe)
    
    class Meta:
        ordering = ['book']
        unique_together = ('book', 'gruppe')

    def save(self, *args, **kwargs):
        if self.is_active:
            Books.objects.filter(gruppe=self.gruppe).exclude(pk=self.pk).update(is_active=False)
        try:
            neu = Books.objects.get(pk=self.pk)
            if neu.slug == '':
                neu.slug = slugify(neu.book)
            orig = new = neu.slug
            for x in itertools.count(1):
                if not Books.objects.filter(gruppe=self.gruppe).filter(slug=new).exclude(pk=self.pk).exists():
                    break
                new = '%s-%d' % (orig, x)
            self.slug = new
        except Books.DoesNotExist:
            # if self.pk is not None:  # nur ausführen bei neu
            if self.slug == '':
                self.slug = slugify(self.book)
            orig = new = self.slug
            for x in itertools.count(1):
                if not Books.objects.filter(gruppe=self.gruppe).filter(slug=new).exclude(pk=self.pk).exists():
                    break
                new = '%s-%d' % (orig, x)
            self.slug = new
        models.Model.save(self, *args, **kwargs)


class BooksTemp(models.Model):
    """Book where the Files (Dateien) are in"""
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    book = models.CharField(max_length=32, db_index=True)
    bookimpid = models.UUIDField()
    slug = models.SlugField(max_length=36, null=True, blank=True)
    gruppe = models.ForeignKey(Group, on_delete=models.SET_NULL, null=True, db_index=True)
    bookroot = models.CharField(max_length=256, default='', blank=True)
    directory = models.CharField(max_length=126, default='', blank=True)
    datum_xml = models.DateTimeField()
    datum_created = models.DateTimeField(auto_now_add=True, editable=False)
    # datum_updated = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        return "Book: {0} / Gruppe: {1}".format(self.book, self.gruppe)

    class Meta:
        ordering = ['book']
        unique_together = ('book', 'gruppe')


class Dateien(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    book = models.ForeignKey('Books', on_delete=models.CASCADE, db_index=True)
    slug = models.SlugField(max_length=200)
    label = models.CharField(max_length=200, null=False, blank=False)  # original name
    dmsname = models.CharField(max_length=255, null=False, blank=False, default='')  # name in filesystem
    hinweis = models.TextField(max_length=20000, default='', blank=True)
    # content = HTMLField()
    # pdf2text; extra table as pdf is not general used ?
    # keyword = models.ForeignKey('KeyWords', on_delete=models.SET_NULL, null=True, blank=True, db_index=True)
    pfad = models.ForeignKey('Url', on_delete=models.SET_NULL, null=True, blank=True, db_index=True)
    is_active = models.BooleanField(default=False, db_index=True)
    dateiimpid = models.UUIDField(null=True, blank=True)
    datum_created = models.DateTimeField(auto_now_add=True, editable=False)
    datum_updated = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        return "Book: {0} / Datei: {1}".format(self.book, self.label)

    class Meta:
        unique_together = ('book', 'slug')  # für den Import
        ordering = ['label']  # ordering = ['book', 'label']
        verbose_name = 'Datei'
        verbose_name_plural = 'Dateien'

    def save(self, *args, **kwargs):
        # if self.pk is not None:  # muss immer ausgeführt werden
        if self.slug == '':
            self.slug = slugify(self.label)
        orig = self.slug
        for x in itertools.count(1):
            if not Dateien.objects.filter(slug=self.slug).exclude(pk=self.pk).exists():
                break
            self.slug = '%s-%d' % (orig, x)
        # je Book soll / darf nur eine Notiz aktiv sein
        if self.is_active:
            if not Dateien.objects.get(pk=self.pk).is_active:
                Dateien.objects.filter(book=self.book).exclude(pk=self.pk).update(is_active=False)
        models.Model.save(self, *args, **kwargs)


class Schluessel(models.Model):
    """
    Table for KeyWords = Schluessel
    """
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    book = models.ForeignKey('Books', on_delete=models.CASCADE, db_index=True)
    schluessel = models.CharField(max_length=100, db_index=True)  # unique ??
    slug = models.SlugField(max_length=200)
    schluesselimpid = models.UUIDField(null=True, blank=True)
    gruppe = models.ForeignKey(Group, on_delete=models.CASCADE, null=True, db_index=True)
    # datei = models.ForeignKey('Dateien', on_delete=models.SET_NULL, null=True, db_index=True)
    datum_created = models.DateTimeField(auto_now_add=True, editable=False)
    datum_updated = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        return self.slug

    class Meta:
        unique_together = (('gruppe', 'slug'), ('gruppe', 'schluessel'))

    def save(self, *args, **kwargs):
        # if self.pk is not None:  # muss immer ausgeführt werden
        orig = self.schluessel
        for x in itertools.count(1):
            if not Schluessel.objects.filter(gruppe=self.gruppe).filter(schluessel=self.schluessel).exclude(
                    pk=self.pk).exists():
                break
            self.schluessel = '%s-%d' % (orig, x)
        if self.slug == '':
            self.slug = slugify(self.schluessel)
        orig = self.slug
        for x in itertools.count(1):
            if not Schluessel.objects.filter(gruppe=self.gruppe).filter(slug=self.slug).exclude(pk=self.pk).exists():
                break
            self.slug = '%s-%d' % (orig, x)
        models.Model.save(self, *args, **kwargs)


class Schluessel2Datei(models.Model):
    """
    Tabelle connects Schluessel (KeyWord) to Datei
    """
    datei = models.ForeignKey('Dateien', on_delete=models.CASCADE)
    schluessel = models.ForeignKey('Schluessel', on_delete=models.CASCADE)


class Url(models.Model):
    """ URL (Tree path zu slug """
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    book = models.ForeignKey('Books', on_delete=models.CASCADE, db_index=True)
    slug = models.CharField(max_length=255, db_index=True)  # , unique=True)
    pfad = models.CharField(max_length=255)  # , unique=True)  # mit Book
    ebene = models.PositiveIntegerField(default=0, db_index=True)  # Tree Ebene
    auf = models.PositiveIntegerField(default=0, db_index=True)  # default=1 könnte alles einblenden
    parent = models.CharField(max_length=255, db_index=True, blank=True)  # muss den slug des parents enthalten
    urlimpid = models.UUIDField(null=True, blank=True)
    filesubpath = models.CharField(max_length=255, db_index=True, blank=True, default='')
    datum_created = models.DateTimeField(auto_now_add=True, editable=False)
    datum_updated = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        # return "{0}".format(self.slug)  # , self.path)
        return self.slug

    class Meta:
        ordering = ['book', 'pfad']
        unique_together = (('book', 'slug'), ('book', 'pfad'))

    def save(self, *args, **kwargs):
        if self.slug == '':
            self.slug = slugify(self.pfad)
        orig = self.slug
        for x in itertools.count(1):
            if not Url.objects.filter(book=self.book).filter(slug=self.slug).exclude(pk=self.pk).exists():
                break
            self.slug = '%s-%d' % (orig, x)
        models.Model.save(self, *args, **kwargs)
