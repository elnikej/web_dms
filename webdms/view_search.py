from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render
from django.urls import reverse
from django.utils.datastructures import MultiValueDictKeyError
from webdms.models import Dateien
from webdms.tools import booknavi, pfadnavi


@login_required
def search_view(request):  # , pathslug):
    """
    Suche mit pathslug damit das Menü aktiv bleibt
    :param request:
    # param pathslug:
    :return:
    """
    schluessellist, booklist, prime = booknavi(request.user.gruppe.group_id)
    pathlist, emptylist = pfadnavi(prime)
    # https://www.calazan.com/adding-basic-search-to-your-django-site/
    dateilist = []
    allebooks = btree = btitel = False
    suchenach = ''
    if request.method == 'GET':  # 'POST':
        datag = request.GET.copy()
        if 'search' in datag:
            suchenach = datag['search']
            # Um in allen Büchern zu suchen
            if 'allebooks' in datag:
                allebooks = datag['allebooks']
            # in Titel suchen
            btitel = False
            try:
                if str(datag['titel']) == 'on':
                    btitel = True
            except MultiValueDictKeyError:
                btitel = False
            # denkbar wäre auch im Tree zu suchen
            # es werden nur Notizen gefunden
            btree = False
            try:
                if str(datag['stree']) == 'on':
                    btree = True
            except MultiValueDictKeyError:
                btree = False
            if allebooks:
                if btitel and btree:
                    dateilist = Dateien.objects.filter(book__in=booklist).filter(
                        Q(label__icontains=suchenach) | Q(pfad__pfad__icontains=suchenach) | Q(
                            hinweis__icontains=suchenach))
                elif btitel:
                    dateilist = Dateien.objects.filter(book__in=booklist).filter(
                        Q(label__icontains=suchenach) | Q(hinweis__icontains=suchenach))
                elif btree:
                    dateilist = Dateien.objects.filter(book__in=booklist).filter(
                        Q(pfad__pfad__icontains=suchenach) | Q(hinweis__icontains=suchenach))
                else:
                    dateilist = Dateien.objects.filter(book__in=booklist).filter(hinweis__icontains=suchenach)
            else:
                if btitel and btree:
                    dateilist = Dateien.objects.filter(book=prime).filter(
                        Q(label__icontains=suchenach) | Q(pfad__pfad__icontains=suchenach) | Q(
                            hinweis__icontains=suchenach))
                elif btitel:
                    dateilist = Dateien.objects.filter(book=prime).filter(
                        Q(label__icontains=suchenach) | Q(hinweis__icontains=suchenach))
                elif btree:
                    dateilist = Dateien.objects.filter(book=prime).filter(
                        Q(pfad__pfad__icontains=suchenach) | Q(hinweis__icontains=suchenach))
                else:
                    dateilist = Dateien.objects.filter(book=prime).filter(hinweis__icontains=suchenach)
    actionurl = reverse('webdms:search', args=[])
    dictionary = {
        'booklist': booklist,
        'book': prime,
        'dateilist': dateilist,
        'emptylist': emptylist,
        'schluessellist': schluessellist,

        'angemeldet': True,
        'error': False,
        'add': True,
        'titel': 'Suchen in Notizen und Menü',
        'pathlist': pathlist,
        # 'pathslug': pathslug,
        'allebooks': allebooks,
        'btree': btree,
        'btitel': btitel,
        'suchtext': suchenach,
        'actionurl': actionurl,
    }
    return render(request, 'suche_main-page.html', dictionary)
