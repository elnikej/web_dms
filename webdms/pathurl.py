from .models import Dateien


def dateiurl(prime, urlslug):
    """
    Liefert Liste mit Dateien zur URL
    :param prime:
    :param urlslug:
    :return:
    """
    return Dateien.objects.filter(book=prime).filter(pfad__slug=urlslug)


def dateiurlempty(prime):
    """
    Liefert Liste mit Dateien ohne URL
    :param prime:
    :return:
    """
    return Dateien.objects.filter(book=prime).filter(pfad__slug=None)
