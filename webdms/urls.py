from django.urls import path, include
# from django.contrib import admin
from .view_book import change_book, book_view, book_add, book_edit, book_backup, book_restore, \
    book_viewtemp, book_media
from .view_keywords import schluessel_add, schluessel_edit, schluessel_find, schluessel_view, schluessel_view_notes
from .view_note import datei_view, datei_add, datei_change, note_delete, datei_fehler, datei_view_book
from .view_url import tree_error, url_change, url_add, menu_add
from .views import index_view, tree_empty, menu_view
from .view_search import search_view
app_name = 'webdms'


# webdms:
urlpatterns = [
    # 'webdms/'
    path('', index_view, name='index'),
    path('menu/', menu_view, name='menu'),
    # path('menu/<str:pathslug>/', menu_add, name='menu_add'),
    path('book/', book_view, name='bookview'),
    path('bookadd/', book_add, name='bookadd'),
    path('bookedit/<slug:bookslug>/', book_edit, name='bookedit'),
    # path('bookdelete/<slug:bookslug>/', book_delete, name='bookdelete'),
    path('bookbackup/', book_backup, name='bookbackup'),
    path('bookrestore/', book_restore, name='bookrestore'),
    path('bookmedia/<str:todo>/<str:dateiname>/', book_media, name='bookmedia'),
    path('booktemp/', book_viewtemp, name='bookviewtemp'),
    path('bookchange/<slug:bookslug>/', change_book, name='changebook'),
    path('path/', include('webdms.wspath.urls', namespace='path')),
    path('tree/<str:pfadslug>/fehlernote/<int:fehlerid>/', datei_fehler, name='notefehler'),
    path('treeadd/', url_add, name='treeadd'),
    path('treeadd/<str:pathslug>/', menu_add, name='menu_add'),
    path('treechange/', url_change, name='treechange'),  # hier wird URL gefüllt
    path('treeempty/', tree_empty, name='treeempty'),
    path('treerror/<int:error>/', tree_error, name='treeerror'),
    path('note/<slug:treeslug>/<slug:noteslug>', datei_view, name='noteview'),
    path('notebook/<slug:bookslug>/<slug:treeslug>/<slug:noteslug>', datei_view_book, name='notesviewbook'),
    path('noteadd/<slug:treeslug>', datei_add, name='noteadd'),
    path('notechange/<slug:noteslug>', datei_change, name='notechange'),
    path('notedelete/<slug:noteslug>', note_delete, name='notedelete'),
    # path('noteimport/<slug:treeslug>', note_import, name='noteimport'),
    # noch nicht fertig
    path('keywords', schluessel_view, name='keywords_view'),
    path('keywordadd', schluessel_add, name='keywordadd'),
    path('keyword/<slug:keyslug>/', schluessel_edit, name='keywordsedit'),
    path('keywordfind/<slug:keyslug>/', schluessel_find, name='schluesselfind'),
    path('keywordview/<slug:bookslug>/<slug:keyslug>/<slug:dateislug>', schluessel_view_notes, name='keywordsviewnotes'),
    path('suchen/', search_view, name='search'),
]
