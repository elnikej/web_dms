from django.apps import AppConfig


class WebdmsConfig(AppConfig):
    name = 'webdms'
