from datetime import datetime
import codecs
from os import path, makedirs
from django.conf import settings
from django.utils.text import slugify

# mediadir = path.join(settings.MEDIA_ROOT, 'import')
mediadir = path.join(settings.MEDIA_ROOT)


def handle_uploaded_media(f, name, typ, subverz='import'):
    """ Um Datei von Form auf Server zu speichern // vorher in string_helper.py
    :param f: Datei {InMemoryUploadedFile}
    :param name: Username  {SimpleLazyObject}
    :param typ: Name {str}
    :param subverz: Unterverzeichnis
    :return: Dateiname mit Pfad
    """
    pfadusername = path.join(mediadir, subverz, str(name))
    if not path.exists(pfadusername):
        makedirs(pfadusername)
    sfn = str(f)  # Problem 2016_Köln.qif mit ö
    filename, file_extension = path.splitext(sfn)
    sfnok = slugify(filename) + file_extension
    # hier test des Namens und ggf Namen anpassen, aber nicht Endung gesondert weshalb slugify nicht geht
    datum = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    dateiname = datum + '_' + str(typ) + '_' + sfnok
    filename = path.join(pfadusername, dateiname)
    with open(filename, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    return filename


def try_utf8(filename):
    """ True wenn filename ein UTF-8 kodierte Datei ist, sonst False
    :param filename: Dateiname
    """
    try:
        #  https://stackoverflow.com/questions/38953533/codecs-open-create-txt-file-in-specific-path-python
        # with codecs.open(fullpath, mode="w", encoding="utf-16") as f:
        #     f.write(u"[HELLO] *ASDASD* /(&) \n")
        with codecs.open(filename, encoding='utf-8', errors='strict') as f:
            for line in f:
                # print(str(line))
                pass
        return True
    except UnicodeDecodeError:
        return False
