**web_dms**

DMS build with django web service

---

## Prepare a development environment
----------------------------------

1. Tested with python 3.6
2. Create virtualenv for webdms:  ``python3 -m venv ~/venv/webdms``
3. Activate the virtual environment ``source ~/venv/webdms/bin/activate``
4. Update the Python package manager ``pip install -U pip``
5. Install webdms requirements in virtualenv: ``pip install -r requirements.txt``
6. Start with sqlite or configure and create Your database
7. Create the database tables with ``./manage.py migrate``
8. Create a superuser for login ``./manage.py createsuperuser``
9. Start the develpment server with ``./manage.py runserver``
10. add user to groups in admin interface

---

## commands

1. django-admin startproject django_start
2. python manage.py startapp basic
3. python manage.py startapp dmsapp
4. python manage.py startapp webdms

---

## functions / state -- in development

1. documents stored as files in books + tree menu
2. search for files 
3. backup & restore
4. convert pdf to searchable pdf with OCRmyPDF -- not implemented
5. convert picture to pdf -- not implemented
